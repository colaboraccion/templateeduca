var myTheme = {
	init : function(){
		var ie_v = $exe.isIE();
		if (ie_v && ie_v<8) return false;
		setTimeout(function(){
			$(window).resize(function() {
				myTheme.reset();
			});
		},1000);
		var l = $('<p id="nav-toggler"><a href="#" onclick="myTheme.toggleMenu(this);return false" class="hide-nav" id="toggle-nav" title="'+$exe_i18n.hide+'"><span>'+$exe_i18n.menu+'</span></a></p>');
		$("#siteNav").before(l);
		$("#topPagination .pagination").prepend('<a href="#" onclick="window.print();return false" title="'+$exe_i18n.print+'" class="print-page"><span>'+$exe_i18n.print+'</span></a> ');
		this.addNavArrows();
		this.bigInputs();		
		var url = window.location.href;
		url = url.split("?");
		if (url.length>1){
			if (url[1].indexOf("nav=false")!=-1) {
				myTheme.hideMenu();
				return false;
			}
		}
		myTheme.setNavHeight();
		// We execute this more than once because sometimes the height changes because of the videos, etc.
		setTimeout(function(){
			myTheme.setNavHeight();
		},1000);
		$(window).load(function(){
			myTheme.setNavHeight();
		});
	},
	isMobile : function(){
		try {
			document.createEvent("TouchEvent");
			return true; 
		} catch(e) {
			return false;
		}
	},	
	bigInputs : function(){
		if (this.isMobile()) {
			$(".MultiSelectIdevice,.MultichoiceIdevice,.QuizTestIdevice,.TrueFalseIdevice").each(function(){
				$('input:radio',this).screwDefaultButtons({
					image: 'url("_style_input_radio.png")',
					width: 30,
					height: 30
				});
				$('input:checkbox',this).screwDefaultButtons({
					image: 'url("_style_input_checkbox.png")',
					width: 30,
					height: 30
				});
				$(this).addClass("custom-inputs");
			});
		}		
	},
	addNavArrows : function(){
		$("#siteNav ul ul .daddy").each(
			function(){
				this.innerHTML+='<span> &#9658;</span>';
			}
		);
	},	
	hideMenu : function(){
		$("#siteNav").hide();
		$(document.body).addClass("no-nav");
		myTheme.params("add");
		$("#toggle-nav").attr("class","show-nav").attr("title",$exe_i18n.show);
	},
	setNavHeight : function(){
		var n = $("#siteNav");
		var c = $("#main-wrapper");
		var nH = n.height();
		var cH = c.height();
		var isMobile = $("#siteNav").css("float")=="none";
		if (cH<nH) {
			cH = nH;
			if (!isMobile) {
				var s = c.attr("style");
				if (s) c.css("min-height",cH+"px");
				else c.attr("style","height:auto!important;height:"+cH+"px;min-height:"+cH+"px");
			}
		}
		var h = (cH-nH+24)+"px";
		var m = 0;
		if (isMobile) {
			h = 0;
			m = "15px";
		} else if (n.css("display")=="table") {
			h = 0;
		}
		n.css({
			"padding-bottom":h,
			"margin-bottom":m
		});
	},
	toggleMenu : function(e){
		if (typeof(myTheme.isToggling)=='undefined') myTheme.isToggling = false;
		if (myTheme.isToggling) return false;
		
		var l = $("#toggle-nav");
		
		if (!e && $(window).width()<900 && l.css("display")!='none') return false; // No reset in mobile view
		if (!e) l.attr("class","show-nav").attr("title",$exe_i18n.show); // Reset
		
		myTheme.isToggling = true;
		
		if (l.attr("class")=='hide-nav') {       
			l.attr("class","show-nav").attr("title",$exe_i18n.show);
			$("#siteFooter").hide();
			$("#siteNav").slideUp(400,function(){
				$(document.body).addClass("no-nav");
				$("#siteFooter").show();
				myTheme.isToggling = false;
			}); 
			myTheme.params("add");
		} else {
			l.attr("class","hide-nav").attr("title",$exe_i18n.hide);
			$(document.body).removeClass("no-nav");
			$("#siteNav").slideDown(400,function(){
				myTheme.isToggling = false;
				myTheme.setNavHeight();
			});
			myTheme.params("delete");            
		}
		
	},
	param : function(e,act) {
		if (act=="add") {
			var ref = e.href;
			var con = "?";
			if (ref.indexOf(".html?")!=-1 || ref.indexOf(".htm?")!=-1) con = "&";
			var param = "nav=false";
			if (ref.indexOf(param)==-1) {
				ref += con+param;
				e.href = ref;                    
			}            
		} else {
			// This will remove all params
			var ref = e.href;
			ref = ref.split("?");
			e.href = ref[0];
		}
	},
	params : function(act){
		$("A",".pagination").each(function(){
			myTheme.param(this,act);
		});
	},
	reset : function() {
		myTheme.toggleMenu();        
		myTheme.setNavHeight();
	},
	getCustomIcons : function(){
		// Provisional solution so the user can use the iDevice Editor to choose an icon
		$(".iDevice_header").each(function(){
			var i = this.style.backgroundImage;
			if (i!="") $(".iDeviceTitle",this).css("background-image",i);
			this.style.backgroundImage = "none";
		});
	}
}
$(function () {
    if ($("body").hasClass("exe-web-site")) {
		myTheme.init(); // Call init inside the 'then' block
    }
    myTheme.getCustomIcons();
});

const jsonData = [];


let jsonActual=jsonData;
const resultadosPorPagina = 10;
let paginaActual = 1;
let palabraBuscada=null;
  
const crear_buscador= () => document.addEventListener('DOMContentLoaded', function() {
	// Encuentra la tabla en el documento
	const table = document.querySelector('table');
	table.id="tablaMaster";
	
	// Crea el span contenedor para el encabezado y los botones
	const spanContainer = document.createElement('span');
	spanContainer.style.display = 'flex';
	spanContainer.style.alignItems = 'center';
	spanContainer.style.justifyContent = 'space-between';
	
	// Mueve el título existente al nuevo span contenedor
	const strong = table.querySelector('strong');
	spanContainer.appendChild(strong);
	
	// Crea el span para los elementos de entrada
	const inputGroup = document.createElement('span');
	inputGroup.style.display = 'flex';
	
	// Crea el campo de entrada de texto
	const input = document.createElement('input');
	input.id="input_text"
	input.type = 'text';
	input.style.textAlign = 'center';
	input.style.width = '10rem';
	input.style.flexGrow = '1';
	input.style.border = 'none';
	input.style.borderRadius = '5px';
	input.style.marginRight = '10px';
	input.placeholder = 'Escribe una palabra';
	
	// Crea el botón de búsqueda
	const searchButton = document.createElement('button');
	searchButton.id="searchButton";
	searchButton.textContent = 'Buscar';
	searchButton.style.border = 'none';
	searchButton.style.color = 'white';
	searchButton.style.backgroundColor = '#4b779f';
	searchButton.style.padding = '10px 20px';
	searchButton.style.borderRadius = '5px';
	searchButton.style.cursor = 'pointer';
	searchButton.style.marginRight = '10px';
	
	// Crea el botón de borrar
	const clearButton = document.createElement('button');
	clearButton.id="clearButton";
	clearButton.textContent = 'Borrar';
	clearButton.style.border = 'none';
	clearButton.style.color = 'white';
	clearButton.style.backgroundColor = '#9f1d1d';
	clearButton.style.padding = '10px 20px';
	clearButton.style.borderRadius = '5px';
	clearButton.style.cursor = 'pointer';
	
	// Agrega los elementos de entrada y los botones al span de grupo de entrada
	inputGroup.appendChild(input);
	inputGroup.appendChild(searchButton);
	inputGroup.appendChild(clearButton);
	
	// Agrega el span de grupo de entrada al span contenedor
	spanContainer.appendChild(inputGroup);
	
	// Encuentra la celda de la tabla y limpia su contenido
	const cell = table.querySelector('td');
	cell.innerHTML = '';
	
	// Agrega el span contenedor a la celda de la tabla
	cell.appendChild(spanContainer);
  });

 crear_buscador();

/////// FORMATO PARA LAS TABLAS /////////
 const pintarHeaderTable = (tablas) => {
    tablas.forEach(tabla => {
        // Aplica el estilo a la primera fila de cada tabla
        const firstRow = tabla.querySelector('tr:first-child');
        if (firstRow) {
            firstRow.style.color = "#D5DDE5";
			firstRow.style.color = "#D5DDE5";
			firstRow.style.background = "#263238";
			firstRow.style.borderBottom = "4px solid var(--color-header-tablas)";
			firstRow.style.borderRight = "1px solid #343a45";
			firstRow.style.borderTop = "0px";
			firstRow.style.borderLeft = "0px";
			firstRow.style.fontWeight = "100";
			firstRow.style.padding = ".6rem";
			firstRow.style.textAlign = "left";
			firstRow.style.textShadow = "0 1px 1px rgba(0, 0, 0, 0.1)";
			firstRow.style.verticalAlign = "middle";
            firstRow.querySelectorAll('th, td').forEach(cell => {
                cell.style.background = "#263238";
				cell.style.borderBottom = "4px solid var(--color-header-tablas)";
				cell.style.borderRight = "1px solid #343a45";
				cell.style.borderTop = "0px";
				cell.style.borderLeft = "0px";
				cell.style.fontWeight = "100";
				cell.style.padding = ".6rem";
				cell.style.textAlign = "left";
				cell.style.textShadow = "0 1px 1px rgba(0, 0, 0, 0.1)";
				cell.style.verticalAlign = "middle";
            });
        }
    });
}

const tables = document.querySelectorAll('table');
pintarHeaderTable(tables);
const resaltarPalabra = (texto, palabra) => {
	if (!palabra) return texto;
	// Escapa caracteres especiales para evitar problemas con expresiones regulares
	const terminoSeguro = palabra.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	const regex = new RegExp(`(${terminoSeguro})`, 'gi');
	return texto.replace(regex, '<span class="resaltado">$1</span>');
};

////// CREACIÓN Y DISTRIBUCIÓN DE LOS DATOS DEL JS EN UNA TABLA
const renderizarTabla = (data, terminoBusqueda = null) => {
    const tableBody = document.querySelector('#tablaMaster tbody');

	let rows = tableBody.querySelectorAll('tr:not(:first-child)');
    rows.forEach(row => row.remove());

    if (data.length === 0) {
        const row = document.createElement('tr');
        const cell = document.createElement('td');
        cell.setAttribute('colspan', 3);
        cell.style.textAlign = 'center';
        cell.textContent = terminoBusqueda ? `No se encontraron resultados para '${terminoBusqueda}'` : 'No hay resultados disponibles';
        row.appendChild(cell);
        tableBody.appendChild(row);
    } else {
        data.forEach(item => {
            // Primera fila para el ID y la imagen
            const row1 = document.createElement('tr');
            tableBody.appendChild(row1);

            // Celda para el ID que abarca dos filas
            const idCell = document.createElement('td');
            idCell.rowSpan = 2;
            idCell.innerHTML = resaltarPalabra(item.id.toString(), terminoBusqueda);
            row1.appendChild(idCell);

            // Celda para la Imagen centrada
			const imgCell = document.createElement('td');
			imgCell.style.display = 'flex';
			imgCell.style.justifyContent = 'center'; // Centra la imagen horizontalmente
			imgCell.colSpan = 2;

			// Crea el enlace que envolverá la imagen
			const link = document.createElement('a');

			// Determina el URL dependiendo del campo de origen
			let urlToNavigate;
			if (item.ServerEscuela) {
				// Si el ítem proviene de Server Escuela, añade el protocolo y el nombre del dominio
				urlToNavigate = window.location.protocol + '//' + window.location.hostname + item.ServerEscuela;
			} else {
				// Para el campo URL, usa el valor directamente sin modificaciones
				urlToNavigate = item.URL;
			}

			link.setAttribute('href', urlToNavigate);
			link.setAttribute('target', '_blank'); // Opcional: abre el enlace en una nueva pestaña

			// Crea la imagen y la añade al enlace
			const img = document.createElement('img');
			img.src = item.Img;
			img.alt = `Imagen del elemento ${item.id}`;
			img.style.maxWidth = '300px'; // Ajustar según se requiera

			// Añade la imagen al enlace, y luego el enlace a la celda
			link.appendChild(img);
			imgCell.appendChild(link);
			row1.appendChild(imgCell);



            // Segunda fila para el resto de la información
            const row2 = document.createElement('tr');
            const infoCell = document.createElement('td');
            infoCell.colSpan = 2; // La información abarca dos columnas

            // Preparar el texto de Nivel con saltos de línea adecuados
            let nivelTexto = item.Nivel.replace(/\|/g, '<br>').replace(/•/g, (match, offset) => offset ? '<br>•' : '•');
			
            // Info izquierda (excepto Nivel y Descripción)
            const infoIzquierda = document.createElement('div');
            infoIzquierda.style.display = 'inline-block';
            infoIzquierda.style.width = '50%';
            infoIzquierda.style.verticalAlign = 'top';

            // Info derecha (Nivel y Descripción)
            const infoDerecha = document.createElement('div');
            infoDerecha.style.display = 'inline-block';
            infoDerecha.style.width = '50%';
            infoDerecha.style.verticalAlign = 'top';

            // Agrega información a la izquierda

			Object.keys(item).forEach(key => {
				const p = document.createElement('p');
			
				if (key === 'Temas') {
					// Elimina "Temas:" si está al inicio y muestra en negrita
					let contenidoTemas = item[key].replace(/^Temas:\s*/, '');
					p.innerHTML = `<strong>Temas:</strong> ${resaltarPalabra(contenidoTemas, terminoBusqueda)}`;
				} else if (['URL', 'ServerenLinea', 'ServerEscuela'].includes(key)) {
					// Procesa URL, Server en Línea, y Server Escuela
if (item[key]) {
    const a = document.createElement('a');
    let displayKey = key; // Variable para almacenar el nombre que se mostrará
    let hrefValue = item[key]; // Valor predeterminado del href

    // Cambia el nombre de display según el key actual
    if (key === 'ServerenLinea') {
        displayKey = 'Server en Línea';
    } else if (key === 'ServerEscuela') {
        displayKey = 'Server Escuela';
        // Solo para Server Escuela, añade el protocolo y hostname
        hrefValue = window.location.protocol + '//' + window.location.hostname + '/' + item[key];
    }
    // Para URL, usa el valor directamente sin añadir protocolo o hostname
    else if (key === 'URL') {
        displayKey = 'URL';
        // Asegura que el hrefValue ya tiene el valor correcto para URL
    }

    a.setAttribute('href', hrefValue);
    a.setAttribute('target', '_blank');
	a.style.color = 'blue';
    // Utiliza displayKey para el texto visible y hrefValue para el enlace
    let linkText = (key === 'URL') ? hrefValue : window.location.protocol + '//' + window.location.hostname + '' + item[key];
    a.innerHTML = resaltarPalabra(linkText, terminoBusqueda);
    p.innerHTML = `<strong>${displayKey}:</strong> `;
    p.appendChild(a);
}

				} else if (key === 'Objetivos de aprendizaje sugeridos') {
					// Procesa objetivos con "•" y en negrita
					const objetivosHTML = item[key].map((objetivo, index) => {
						const objetivoLimpio = objetivo.trim();
						const prefijo = index > 0 ? `<br>` : `<br>`;
						if (objetivoLimpio && objetivoLimpio !== "Competencias relevantes") {
							return `${prefijo} ${resaltarPalabra(objetivoLimpio, terminoBusqueda)}`;
						} else {
							return `${prefijo}${resaltarPalabra(objetivoLimpio, terminoBusqueda)}`;
						}
					}).join('');
					p.innerHTML = `<strong>Objetivos de aprendizaje sugeridos:</strong> ${objetivosHTML}`;
				} else if (!['id', 'Img', 'Nivel', 'Descripcion', 'URL', 'ServerenLinea', 'ServerEscuela', 'Temas'].includes(key)) {
					// Procesa todos los demás campos en negrita
					let contenido = Array.isArray(item[key]) ? item[key].join(', ') : item[key];
					p.innerHTML = `<strong>${key}:</strong> ${resaltarPalabra(contenido, terminoBusqueda)}`;
				}
			
				// Añade el párrafo al div de información izquierda o derecha según corresponda
				if (p.innerHTML) { // Asegura que solo se añadan párrafos con contenido
					infoIzquierda.appendChild(p);
				}
			});
			
			// Agrega Nivel y Descripción a la derecha, sin necesidad de repetir la lógica para estos campos
			let descripcionTexto = item.Descripcion ? `<strong>Descripción:</strong> ${resaltarPalabra(item.Descripcion, terminoBusqueda)}` : "";
			
			if (nivelTexto) {
				const nivelP = document.createElement('p');
				nivelP.innerHTML = nivelTexto; // Ya se aplicó resaltarPalabra() previamente si es necesario
				infoDerecha.appendChild(nivelP);
			}
			
			if (descripcionTexto) {
				const descripcionP = document.createElement('p');
				descripcionP.innerHTML = descripcionTexto;
				infoDerecha.appendChild(descripcionP);
			}
			
			// Añade las secciones izquierda y derecha al infoCell y luego al DOM como antes
			infoCell.appendChild(infoIzquierda);
			infoCell.appendChild(infoDerecha);
			row2.appendChild(infoCell);
			tableBody.appendChild(row2);
			

			// Object.keys(item).forEach(key => {
//     const p = document.createElement('p');

// 	if (key === 'Temas') {
//         let contenidoTemas = item[key].replace(/^Temas:\s*/, ''); // Elimina "Temas:" si está al inicio
//         p.innerHTML = `<strong>Temas:</strong> ${resaltarPalabra(contenidoTemas, terminoBusqueda)}`;
//         infoIzquierda.appendChild(p);
//     } else if (['URL', 'ServerenLinea', 'ServerEscuela'].includes(key) && item[key]) {
//         // Crear enlaces para "URL", "Server en Línea" y "Server Escuela"
//         const a = document.createElement('a');
//         a.setAttribute('href', item[key]);
//         a.setAttribute('target', '_blank');
//         a.innerHTML = resaltarPalabra(item[key], terminoBusqueda);
//         // Añade el enlace al párrafo con el título en negrita
//         p.innerHTML = `<strong>${key}:</strong> `;
//         p.appendChild(a);
//         infoIzquierda.appendChild(p);
//     } else if (!['id', 'Img', 'Nivel', 'Descripcion'].includes(key)) {
//         // Manejo general para otros campos
//         let contenido = Array.isArray(item[key]) ? item[key].join(', ') : item[key];
//         // Agregar el título en negrita para campos que no son 'URL', 'ServerenLinea', 'ServerEscuela'
//         if (!['URL', 'ServerenLinea', 'ServerEscuela', 'Temas'].includes(key)) {
//             contenido = `<strong>${key}:</strong> ${contenido}`;
//         }
//         p.innerHTML = resaltarPalabra(contenido, terminoBusqueda);
//         infoIzquierda.appendChild(p);
//     }

// 		// Título en negrita para cada campo excepto 'Temas', 'Nivel' y 'Descripcion'
// 		if (!['id', 'Img', 'Nivel', 'Descripcion', 'Temas'].includes(key)) {
// 			let contenido = `<strong>${key}:</strong> ${item[key]}`;
// 			// Verifica si el campo es uno de los campos URL
// 			if (['URL', 'ServerenLinea', 'ServerEscuela'].includes(key) && item[key]) {
// 				contenido = `<strong>${key}:</strong> <a href="${item[key]}" target="_blank">${item[key]}</a>`;
// 			}
// 			// Aplicar resaltado de búsqueda
// 			p.innerHTML = resaltarPalabra(contenido, terminoBusqueda);
// 			infoIzquierda.appendChild(p);
// 		}
	
// // Verifica si el campo actual es uno de los campos URL
// if (key === 'URL' || key === 'ServerenLinea' || key === 'ServerEscuela') {
// 	// Crea un enlace solo si el campo tiene un valor
// 	if (item[key]) {
// 		const a = document.createElement('a');
// 		a.setAttribute('href', item[key]);
// 		a.setAttribute('target', '_blank'); // Abre el enlace en una nueva pestaña
// 		a.innerHTML = resaltarPalabra(`${key}: ${item[key]}`, terminoBusqueda);
// 		p.appendChild(a); // Añade el enlace al párrafo
// 		infoIzquierda.appendChild(p); // Estos enlaces van en la parte izquierda
// 	}
// } else if (key === 'Objetivos de aprendizaje sugeridos') {
//     // Procesa cada objetivo individualmente
//     const objetivosHTML = item[key].map((objetivo, index) => {
//         const objetivoLimpio = objetivo.trim();
//         // Agrega "•" solo si es necesario, basado en el contenido del objetivo y su posición
//         const prefijo = index > 0 ? `<br>` : '<br>'; // Asegura que el primer elemento no tenga un <br> antes
//         if (objetivoLimpio && objetivoLimpio !== "Competencias relevantes") {
//             return `${prefijo}${resaltarPalabra(objetivoLimpio, terminoBusqueda)}`;
//         } else {
//             // No agrega "•" si la línea está vacía o es el encabezado de "Competencias relevantes"
//             // Maneja el caso especial para el primer elemento sin duplicar el "•"
//             return index > 0 ? `<br>${resaltarPalabra(objetivoLimpio, terminoBusqueda)}` : resaltarPalabra(objetivoLimpio, terminoBusqueda);
//         }
//     }).join('');
//     // Añade el título en negrita antes del contenido de los objetivos
//     p.innerHTML = `<strong>${key}:</strong> ${objetivosHTML}`;
//     infoIzquierda.appendChild(p);
//     } else if (!['id', 'Img', 'Nivel', 'Descripcion'].includes(key)) {
//         let valor = Array.isArray(item[key]) ? item[key].join(' ') : item[key];
//         p.innerHTML = resaltarPalabra(`${key}: ${valor}`, terminoBusqueda);
//         infoIzquierda.appendChild(p);
//     }
// });

// let descripcionTexto = item.Descripcion ? `<strong>Descripción:</strong> ${item.Descripcion}` : "";

// nivelTexto = resaltarPalabra(nivelTexto, terminoBusqueda);
// descripcionTexto = resaltarPalabra(descripcionTexto, terminoBusqueda);

// if (descripcionTexto) {
//     const descripcionP = document.createElement('p');
//     descripcionP.innerHTML = descripcionTexto;
//     infoDerecha.appendChild(descripcionP);
// }
// 			 // Nivel y Descripción directamente a la derecha sin título para Nivel
//  			let nivelContenido = item.Nivel ? item.Nivel : "";
//  			let descripcionContenido = item.Descripcion ? `<strong>Descripción:</strong> ${item.Descripcion}` : "";
 
// 			// Aplicar resaltado y ajustar Nivel
// 			nivelContenido = resaltarPalabra(nivelContenido, terminoBusqueda);
// 			descripcionContenido = resaltarPalabra(descripcionContenido, terminoBusqueda);

// 			  // Agrega Nivel y Descripción a la derecha
// 			  const nivelP = document.createElement('p');
// 			  nivelP.innerHTML = resaltarPalabra(`${nivelTexto}`, terminoBusqueda);
// 			  infoDerecha.appendChild(nivelP);

//             if (descripcionContenido) {
//                 const descripcionP = document.createElement('p');
//                 descripcionP.innerHTML = descripcionContenido;
//                 infoDerecha.appendChild(descripcionP);
//             }
          

//             // const descripcionP = document.createElement('p');
//             // descripcionP.innerHTML = resaltarPalabra(`Descripción: ${item.Descripcion}`, terminoBusqueda);
//             // infoDerecha.appendChild(descripcionP);

//             // Añade las mitades a la celda de información y la celda al row2
//             infoCell.appendChild(infoIzquierda);
//             infoCell.appendChild(infoDerecha);
//             row2.appendChild(infoCell);
//             tableBody.appendChild(row2);
        });
    }
	// Luego de crear las mini tablas, aplica los estilos de encabezado
	pintarHeaderTable(document.querySelectorAll('.minitable'));
};	

// document.addEventListener("DOMContentLoaded", () =>renderizarTabla(jsonData));

// Función para actualizar el estado de los botones de paginación
const actualizarEstadoBotones = (paginaActual, maxPaginas) => {
	const botonAnterior = document.getElementById('anterior');
	const botonSiguiente = document.getElementById('siguiente');
  
	if (botonAnterior) {
	  botonAnterior.classList.toggle('disabled', paginaActual <= 1);
	  botonAnterior.disabled = paginaActual <= 1; // Deshabilita el botón si estamos en la primera página
	}
  
	if (botonSiguiente) {
	  botonSiguiente.classList.toggle('disabled', paginaActual >= maxPaginas);
	  botonSiguiente.disabled = paginaActual >= maxPaginas; // Deshabilita el botón si estamos en la última página
	}
  };

// Función para cambiar la página
const cambiarPagina = (pagina, jsonActual, terminoBusqueda=null) => {
	let inicio = (jsonActual.length >=1) ? ((pagina - 1) * resultadosPorPagina) : 1; //Si la página tiene más de 1 elemento se irá actualizando, sino, comenzará en 1
	let fin=1;
	if(jsonActual.length===0){
		fin =1;
	}
	else if(jsonActual.length>=10){
		fin =(inicio + resultadosPorPagina);
	}else{
		fin=jsonActual.length;
	}

	const datosPagina = jsonActual.slice(inicio, fin);
	
	renderizarTabla(datosPagina, terminoBusqueda);
	actualizarEstadoBotones(pagina, Math.ceil(jsonActual.length / resultadosPorPagina));

};

const reiniciarTabla = () => {
	jsonActual=jsonData;
	paginaActual=1;
	palabraBuscada=null;
	cambiarPagina(paginaActual,jsonActual);
  };

// Inicialmente, muestra la primera página
document.addEventListener("DOMContentLoaded", async ()=>{
//conseguir datos aquí
try{
	await fetch('./salida.json')
	.then((response) => response.json())
	.then((data) => {
		jsonData.push(...data);	
		
	})
	.catch((error) => {
		console.error('Error fetching or parsing JSON:', error);
	});
}catch{

}
 
	cambiarPagina(paginaActual,jsonActual);
});
// Función para crear la paginación en un div al final de la tabla
const crearPaginacion = () => {
	const divPaginacion = document.createElement('div');
	divPaginacion.id = 'paginacion';
  
	// Agrega botones "Anterior" y "Siguiente"
	const botonAnterior = document.createElement('button');
	botonAnterior.textContent = 'Anterior';
	botonAnterior.addEventListener('click', () => {
	  if (paginaActual > 1) {
		paginaActual--;
		cambiarPagina(paginaActual,jsonActual, palabraBuscada);
	  }
	});
  
	const botonSiguiente = document.createElement('button');
	botonSiguiente.textContent = 'Siguiente';
	botonSiguiente.addEventListener('click', () => {
	  const maxPaginas = Math.ceil(jsonActual.length / resultadosPorPagina);
	  if (paginaActual < maxPaginas) {
		paginaActual++;
		cambiarPagina(paginaActual, jsonActual, palabraBuscada);
	  }
	});
	botonAnterior.id = 'anterior';
  	botonSiguiente.id = 'siguiente';
	divPaginacion.appendChild(botonAnterior);
	divPaginacion.appendChild(botonSiguiente);
  
	// Agrega el div de paginación al final de la tabla
	const tabla = document.querySelector('#tablaMaster');
	tabla.parentNode.appendChild(divPaginacion);
	// Actualiza el estado de los botones inmediatamente después de crearlos
	actualizarEstadoBotones(paginaActual, Math.ceil(jsonActual.length / resultadosPorPagina));
  };
  
// Llama a la función para crear la paginación
document.addEventListener("DOMContentLoaded", ()=> crearPaginacion());

// Agrega eventos a los botones de navegación
const botonAnterior = document.querySelector('#anterior');
const botonSiguiente = document.querySelector('#siguiente');
  
const actualizarTablaConResultados = (resultadosFiltrados, terminoBusqueda) => {
	paginaActual = 1;
	palabraBuscada=terminoBusqueda
	// Asumiendo que los estilos de encabezado ya están aplicados y no necesitan ser reaplicados cada vez.
	cambiarPagina(paginaActual,resultadosFiltrados, palabraBuscada);
};

const buscarEnjsonData = () => {
	// Obtén el texto de búsqueda
	const textoDeBusqueda = document.getElementById('input_text').value.trim().toLowerCase();

	if (textoDeBusqueda === "") {
        jsonActual = jsonData;
    } else {
		const jsonSearch = jsonData.map(({Img, URL, ServerenLinea, ServerEscuela, ...rest}) => rest);
		
		// Filtra jsonActual para encontrar elementos que coincidan con el texto de búsqueda
		const resultadosFiltrados = jsonSearch.filter(item => {
			return Object.entries(item).some(([key, value]) => {
				// Exclude 'Img' property from the search
					return String(value).toLowerCase().includes(textoDeBusqueda);
			});
		});

        jsonActual = resultadosFiltrados.map(filteredItem => {
			const { id } = filteredItem;
			const originalItem = jsonData.find(item => item.id === id);
			return { ...originalItem, ...filteredItem};
		});
    }
    actualizarTablaConResultados(jsonActual, textoDeBusqueda);
};

const manejarKeypressEnInput = (evento) => {
	// Verifica si la tecla presionada es 'Enter'
	if (evento.key === 'Enter') {
		// Evita que se ejecute la acción predeterminada de la tecla Enter
		evento.preventDefault();

		// Llama a la función de búsqueda
		buscarEnjsonData();
	}
};




// Función asíncrona para esperar a que crear_buscador se complete antes de agregar el evento al botón
const inicializarBuscador = async () => {
	await crear_buscador(); 

	// Ahora que crear_buscador ha terminado, se puede agregar el evento al botón de búsqueda
	const searchButton = document.getElementById('searchButton');
	searchButton.addEventListener('click', buscarEnjsonData);

	 // Agrega el evento de clic al botón de limpiar
	 const clearButton = document.getElementById('clearButton');
	 clearButton.addEventListener('click',() => {
		const inputText = document.getElementById('input_text');
		inputText.value='';
		reiniciarTabla();
	
	});

	 // Agrega el evento de teclado al campo de entrada de texto
	 const inputText = document.getElementById('input_text');
	 inputText.addEventListener('keypress', manejarKeypressEnInput);
};

// Evento para cuando el DOM esté completamente cargado
document.addEventListener('DOMContentLoaded', inicializarBuscador);  


const nodeTitle = document.getElementById("nodeTitle");

// Función para actualizar el valor de left de #nodeDecoration
const updateNodeDecorationLeft=()=> {
	
	const nodeDecoration = document.getElementById("nodeDecoration");
	const siteNav = document.getElementById("siteNav");
	// Ancho actual de #siteNav
	const siteNavWidth = siteNav.offsetWidth;

	// Verificar si el ancho de #siteNav es mayor que 172px
	if (siteNavWidth > 172) 
	{
		// Calcula la diferencia entre el ancho actual y 172px
		const widthDifference = siteNavWidth - 172;

		// Conviertir la diferencia a "rem" (asumiendo que 1px = 0.0625rem)
		const widthDifferenceInRem = widthDifference * 0.0625;

		// Calcular el nuevo valor de left como la suma de 13rem y la diferencia en "rem"
		const newLeftValue = 13 + widthDifferenceInRem;

		// Establecer el nuevo valor de left
		nodeDecoration.style.left = newLeftValue + "rem";
	}
	else{
		nodeDecoration.style.left = "13rem";
	}
	
}

const changeLeft_nodeDecoration=() =>{
    const nodeDecoration = document.getElementById("nodeDecoration");
    const siteNav = document.getElementById("siteNav");
    

    // Verificar si #siteNav está visible
    if (siteNav.style.display !== "block") {
		// Verificar el valor real de siteNav.style.display
		console.log(siteNav.style.display);
        // Si #siteNav está oculto, establecer el left en 1rem
        nodeDecoration.style.left = "1rem";
    }
	else{
		updateNodeDecorationLeft()
	}

}

// Función para manejar el cambio de tamaño del elemento "nodeTitle"
const handleNodeTitleResize= () => {
	const article= document.querySelector('article')
    const nodeTitleHeight = nodeTitle.offsetHeight; // Obtener la altura del elemento en px
	
    // Calcular el margin-top deseado (4rem + altura del elemento)
    const marginTopInRem = 4 + (nodeTitleHeight / 16) -3; // Suponiendo que 1rem = 16px

    // Verificar si el tamaño de fuente es mayor a 41px
    if (nodeTitleHeight > 41) {
        // Aplicar el nuevo margin-top en rem
        article.style.marginTop = `${marginTopInRem}rem`;
    } else {
        // Restablecer el margin-top si el tamaño de fuente es menor o igual a 41px
        article.style.marginTop = "1rem";
    }
}



// Función que se ejecutará cuando se detecten cambios en el elemento "siteNav"
function handleMutations(mutationsList) {
	const siteNav = document.getElementById('siteNav');
	const nodeDecoration = document.getElementById('nodeDecoration');
	const header = document.getElementById('header');


    for(let mutation of mutationsList) {
        // Verifica si el atributo "style" ha cambiado
        if (mutation.attributeName === 'style'|| (mutation.attributeName === 'orientation' && mutation.type === 'attributes')) {
			const isPortrait = window.matchMedia("(orientation: portrait").matches;
			const isSiteNavHidden = window.getComputedStyle(siteNav).display === 'none';
			const valor = window.getComputedStyle(nodeDecoration);
            // const siteNav = mutation.target;
            // Verifica si el valor de "display" es "none"
            if (isSiteNavHidden) {
				if(isPortrait){
					const screenHeight = screen.height;
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					//siteNav inactivo, orientación vertical
					// Si es "none", modifica el valor de "left" del elemento "header" con id "nodeDecoration"
					nodeDecoration.style.left = '1rem';
					nodeDecoration.style.top = `${calculatedTop}px`;
					 console.log("inactivo vertical");
					 console.log(valor.top);
					 console.log("valor calculado: ", calculatedTop);
				}else{
					const screenHeight = screen.height;
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					//siteNav inactivo y orientación horizontal
					nodeDecoration.style.left = '1rem';
					nodeDecoration.style.top = `${calculatedTop}px`;
					 console.log(valor.top);
					 console.log("inactivo horizontal");
					
				}
                
                // const nodeDecoration = document.getElementById('nodeDecoration');
                // nodeDecoration.style.left = '1rem';
				
            }
			else{
				if(isPortrait){
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					// siteNav activo, orientación vertical
					nodeDecoration.style.left = 'calc(16rem + var(--tamano-body)*0.06)';
                    nodeDecoration.style.top = `${calculatedTop}px`;
					console.log("activo vertical");
					console.log(valor.top);
				}else{
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					// siteNav activo, orientación horizontal
					nodeDecoration.style.left = "calc(16rem + var(--tamano-body)*0.06)";
                    nodeDecoration.style.top = `${calculatedTop}px`;
					console.log("activo horizontal");
					console.log(valor.top);
				}
				// const nodeDecoration = document.getElementById('nodeDecoration');
			}
        }
    }
}

// Selecciona el elemento con id "siteNav"
const siteNav = document.getElementById('siteNav');

// Crea una nueva instancia de MutationObserver
const observer = new MutationObserver(handleMutations);

// Configura el observer para escuchar cambios en el atributo "style"
observer.observe(siteNav, { attributes: true, attributeFilter: ['style'] });

// Agregar un evento de cambio de tamaño a window para manejar el cambio de tamaño de nodeTitle
window.addEventListener("resize", handleNodeTitleResize());


/* PINTAR EL HEADER DE COLOR BLANCO CUANDO SE HAGA UN SCROLL HACIA ABAJO */

const handleScroll = () =>{
	const nodeDecoration = document.getElementById('nodeDecoration');
	const targetScroll = 10;

	if (document.getElementById('main').scrollTop >= targetScroll) {
		nodeDecoration.style.background = 'white';

	} else {
		nodeDecoration.style.background = 'white';
	}
}

document.getElementById('main').addEventListener('scroll', handleScroll);

/**
 * Scroll Persistence for #siteNav
 * 
 * Este script permite que, al seleccionar un enlace dentro de #siteNav,
 * la posición de desplazamiento se guarde en localStorage. Al recargar
 * la página, el nav se desplazará automáticamente a la posición guardada,
 * mostrando el enlace seleccionado en primer lugar.
 */
document.addEventListener("DOMContentLoaded", function() {
    // Obtener el elemento siteNav
    var siteNav = document.getElementById("siteNav");

    // Si existe siteNav y localStorage tiene la posición guardada, desplázate a esa posición
    if (siteNav && localStorage.getItem("scrollPosition")) {
        siteNav.scrollTop = localStorage.getItem("scrollPosition");
    }

    // Agregar un evento de clic a todos los enlaces dentro de siteNav
    var navLinks = siteNav.querySelectorAll("a");
    navLinks.forEach(function(link) {
        link.addEventListener("click", function() {
            // Al hacer clic en un enlace, almacena la posición de desplazamiento actual en localStorage
            localStorage.setItem("scrollPosition", siteNav.scrollTop);
        });
    });
});

/* En dispositivos móbiles el #PackageLicense solo se muestra 
si el valor del margin-bottom es de 10.5em en orientación horizontal
 y se muestra a partir de 8.5em en orientacion vertical */

/* La funcion handleLicense debe cambiar dicho valor dependiendo de la orientación del dispositivo */ 
function handleLicense() {
	const packageLicense = document.getElementById("packageLicense");
  
	const setMarginBottom = () => {
	  const isPortrait = window.matchMedia("(orientation: portrait)").matches;
	  if (isPortrait) {
		//Si es vertical marginbottom es 8.5em
		console.log("Orientación vertical");
		packageLicense.style.marginBottom = '12rem';
		console.log(packageLicense.style.marginBottom)
	  } else {
		//Si es horizontal marginbottom es 10.5em
		console.log("Orientación horizontal");
		packageLicense.style.marginBottom = '10.5em';
		console.log(packageLicense.style.marginBottom)
	  }
	};
  
	setMarginBottom(); 
  
	window.addEventListener('resize', setMarginBottom);
	window.addEventListener('orientationchange', setMarginBottom);
  }
  
  handleLicense(); // Llamada inicial para establecer el margen de acuerdo a la orientación inicial


/*!
 * ScrewDefaultButtons v2.0.6
 * http://screwdefaultbuttons.com/
 *
 * Licensed under the MIT license.
 * Copyright 2013 Matt Solano http://mattsolano.com
 *
 * Date: Mon February 25 2013
 */(function(e,t,n,r){var i={init:function(t){var n=e.extend({image:null,width:50,height:50,disabled:!1},t);return this.each(function(){var t=e(this),r=n.image,i=t.data("sdb-image");i&&(r=i);r||e.error("There is no image assigned for ScrewDefaultButtons");t.wrap("<div >").css({display:"none"});var s=t.attr("class"),o=t.attr("onclick"),u=t.parent("div");u.addClass(s);u.attr("onclick",o);u.css({"background-image":r,width:n.width,height:n.height,cursor:"pointer"});var a=0,f=-n.height;if(t.is(":disabled")){a=-(n.height*2);f=-(n.height*3)}t.on("disableBtn",function(){t.attr("disabled","disabled");a=-(n.height*2);f=-(n.height*3);t.trigger("resetBackground")});t.on("enableBtn",function(){t.removeAttr("disabled");a=0;f=-n.height;t.trigger("resetBackground")});t.on("resetBackground",function(){t.is(":checked")?u.css({backgroundPosition:"0 "+f+"px"}):u.css({backgroundPosition:"0 "+a+"px"})});t.trigger("resetBackground");if(t.is(":checkbox")){u.on("click",function(){t.is(":disabled")||t.change()});u.addClass("styledCheckbox");t.on("change",function(){if(t.prop("checked")){t.prop("checked",!1);u.css({backgroundPosition:"0 "+a+"px"})}else{t.prop("checked",!0);u.css({backgroundPosition:"0 "+f+"px"})}})}else if(t.is(":radio")){u.addClass("styledRadio");var l=t.attr("name");u.on("click",function(){!t.prop("checked")&&!t.is(":disabled")&&t.change()});t.on("change",function(){if(t.prop("checked")){t.prop("checked",!1);u.css({backgroundPosition:"0 "+a+"px"})}else{t.prop("checked",!0);u.css({backgroundPosition:"0 "+f+"px"});var n=e('input[name="'+l+'"]').not(t);n.trigger("radioSwitch")}});t.on("radioSwitch",function(){u.css({backgroundPosition:"0 "+a+"px"})});var c=e(this).attr("id"),h=e('label[for="'+c+'"]');h.on("click",function(){u.trigger("click")})}if(!e.support.leadingWhitespace){var c=e(this).attr("id"),h=e('label[for="'+c+'"]');h.on("click",function(){u.trigger("click")})}})},check:function(){return this.each(function(){var t=e(this);i.isChecked(t)||t.change()})},uncheck:function(){return this.each(function(){var t=e(this);i.isChecked(t)&&t.change()})},toggle:function(){return this.each(function(){var t=e(this);t.change()})},disable:function(){return this.each(function(){var t=e(this);t.trigger("disableBtn")})},enable:function(){return this.each(function(){var t=e(this);t.trigger("enableBtn")})},isChecked:function(e){return e.prop("checked")?!0:!1}};e.fn.screwDefaultButtons=function(t,n){if(i[t])return i[t].apply(this,Array.prototype.slice.call(arguments,1));if(typeof t=="object"||!t)return i.init.apply(this,arguments);e.error("Method "+t+" does not exist on jQuery.screwDefaultButtons")};return this})(jQuery);

 