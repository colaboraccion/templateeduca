const fs=require('fs');
const readline = require('readline');

let jsonStruct={
    "id":0,
    "Img":"",
    "Simulador":"",
    "URL":"",
    "ServerenLinea":"",
    "ServerEscuela":"",
    "Temas": "",
    "Objetivos de aprendizaje sugeridos": "",
    "Nivel":"",
    "Descripcion":"",
}

let lista_mat_simulador=[]
let id= 0;
// Función para procesar el texto y convertirlo a JSON
const convertirTextoAJson=(texto) =>{
    
// Incrementamos el id para la nueva sección
id++;

// Usamos la estructura existente como base
let json = { ...jsonStruct, id };
// if(texto.includes('Server Escuela:/contents/')){
//     console.log(texto);
// }
// Dividimos el texto en líneas
let lineas = texto.trim().replace(/\r\n/g, '\n').split('\n');
let serverEscuelaLine = lineas.find(linea => linea.startsWith('Server Escuela:'));
// if (serverEscuelaLine) {
//     console.log('SE:', serverEscuelaLine);
// } else {
//     console.log('No se encontró "Server Escuela:"');
// }

let nivelPrimaria = []; // Para almacenar datos de Escuela Primaria
let nivelSecundaria = []; // Para almacenar datos de Escuela Secundaria

lineas.forEach((linea, indice) => {
    linea = linea.trim(); // Elimina espacios en blanco al principio y al final de la línea

        // Usando switch para manejar diferentes casos
        if (linea.startsWith('Escuela Primaria')) {
            // Procesa líneas hasta encontrar 'El simulador' o el inicio de otra sección
            for (let i = indice + 1; i < lineas.length && !lineas[i].startsWith('El simulador') && !lineas[i].startsWith('Escuela Secundaria'); i++) {
                nivelPrimaria.push(lineas[i].trim());
            }
        } else if (linea.startsWith('Escuela Secundaria')) {
            // Procesa líneas hasta encontrar 'El simulador' o el inicio de otra sección
            for (let i = indice + 1; i < lineas.length && !lineas[i].startsWith('El simulador'); i++) {
                if (lineas[i].trim().startsWith('Escuela Primaria')) break; // Evita que se sobreponga con Escuela Primaria
                nivelSecundaria.push(lineas[i].trim());
            }
        }


    // console.log(id);
    switch (true) {
        default:
            // console.log(lineas[0]);
            if (lineas[0] === '1'){
                json['Simulador'] = lineas[1];
            } else{
                json['Simulador'] = lineas[0];
            }
            break;
        

        case linea.startsWith('URL: '):
        for (let i = indice; i < lineas.length; i++) {
            const lineaReferencia = lineas[i]?.trim();
            
            // Break if the line starts with 'Temas: '
            if (lineaReferencia.startsWith('Temas: ')) {
                break;
            }else if(lineaReferencia.startsWith("URL: ")){
                json['URL'] = lineaReferencia.replace('URL: ', '').trim();
            }
        }
        break;
        case linea.startsWith('Server Escuela:'):
            for(let i = indice; i<lineas.length; i++){
                const lineaReferencia2 = lineas[i]?.trim();
            if(lineaReferencia2.startsWith('Temas: ')){
                break;
            }else if (lineaReferencia2.startsWith("Server Escuela:")){
                // console.log('entra serverescuela');
                // console.log(lineaReferencia2);
                json['ServerEscuela'] = lineaReferencia2.replace('Server Escuela:', '').trim();
            }
        }
            break;
        case linea.startsWith('Temas: '):
            // console.log(id);
            let TemasEncontrados = [];

            for(let i = indice + 1; i<lineas.length; i++){
                const lineaTemas = lineas[i-1]?.trim();

                if(lineaTemas.startsWith('Objetivos de aprendizaje sugeridos:')){
                    break;
                }
                TemasEncontrados.push(lineaTemas);
            
            json['Temas'] = TemasEncontrados.join(lineaTemas);
            }
            json['Temas'] = TemasEncontrados.join('');

            break;
        
        case linea.startsWith('Objetivos de aprendizaje sugeridos:'):
                let objetivosEncontrados = [];
                for (let i = indice + 1; i < lineas.length; i++) {
                    const lineaObjetivo = lineas[i]?.trim();
                    if (lineaObjetivo.startsWith('Escuela Primaria') || lineaObjetivo.startsWith('Escuela Secundaria')) {
                        break;
                    }
                    objetivosEncontrados.push(lineaObjetivo);
                }
                json['Objetivos de aprendizaje sugeridos'] = objetivosEncontrados;
            
                break;
        
                // case linea.startsWith('Escuela Primaria'):
                //     let nivelEncontrados = [];
                //     for(let i = indice; i < lineas.length; i++){
                //         const lineaNivel = lineas[i]?.trim();
                //         if (lineaNivel.startsWith('El simulador ')) {
                //             break;
                //         }
                //         nivelEncontrados.push(lineaNivel);
                //     }
                //     // Concatena con los datos existentes, si los hay
                //     json['Nivel'] = json['Nivel'] ? json['Nivel'] + ', ' + nivelEncontrados.join(', ') : nivelEncontrados.join(', ');
                //     break;
            
                // case linea.startsWith('Escuela Secundaria'):
                //     let nivelEncontrados2 = [];
                //     for(let i = indice; i < lineas.length; i++){
                //         const lineaNivel2 = lineas[i]?.trim();
                //         if (lineaNivel2.startsWith('El simulador ')) {
                //             break;
                //         }
                //         nivelEncontrados2.push(lineaNivel2);
                //     }
                //     // Concatena con los datos existentes, si los hay
                //     json['Nivel'] = json['Nivel'] ? json['Nivel'] + ', ' + nivelEncontrados2.join(', ') : nivelEncontrados2.join(', ');
                //     break;

        case linea.startsWith('El simulador'):
            
            let areaEncontrados = [];
                for(let i = indice ; i<lineas.length; i++){
                    const lineaArea = lineas[i]?.trim();
                    // console.log(lineaArea);
                    areaEncontrados.push(lineaArea);
                    // console.log(areaEncontrados);

                json['Descripcion'] = areaEncontrados.join(lineaArea);
                }
                json['Descripcion'] = areaEncontrados.join(' ');
                // console.log(areaEncontrados);
                
                break;

            // case linea.startsWith('Universidad') || linea.startsWith('Escuela'):
            //     break;
    }
});

    json['Img'] = getImagePath(id);

    // Actualiza el objeto json según los datos recopilados
    if (nivelPrimaria.length > 0) {
        json['Nivel'] += 'Escuela Primaria: ' + nivelPrimaria.join(' '); // Agrega información de Escuela Primaria
    }
    if (nivelSecundaria.length > 0) {
        if (json['Nivel']) json['Nivel'] += ' | '; // Separa la información de primaria y secundaria si ambas existen
        json['Nivel'] += 'Escuela Secundaria: ' + nivelSecundaria.join(' '); // Agrega información de Escuela Secundaria
    }

    return json;
}

const getImagePath = (id) => {
    // Assuming images are in the "Imagenes_simuladores" folder
    const folderPath = 'Imagenes_simuladores';

    // Construct the file name based on the ID
    const fileName = `${String(id).padStart(2, '0')}-.*-420.png`; // Adjust the pattern based on your naming convention

    // Read the files in the folder
    const files = fs.readdirSync(folderPath);

    // Find the file that matches the pattern
    const matchingFile = files.find(file => new RegExp(fileName).test(file));

    if (!matchingFile) {
        console.error(`Image file not found for ID ${id}`);
        return null;
    }

    // Construct the full image path
    const imagePath = `${folderPath}/${matchingFile}`;

    return imagePath;
};


//Funcion para extraer cada Fila de la tabla que esta como texto
const procesarArchivo= async(rutaArchivo) => {
    // Crear un stream de lectura del archivo. 
    const fileStream = fs.createReadStream(rutaArchivo, {encoding: 'utf8'});

    // Crear una interfaz readline que usará el stream de archivo.
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity // Trata los finales de línea CRLF y LF igual.
    });

    let seccionActual = ""; // Almacena el texto de la sección actual
    let seccionNumero = "";  // Almacena el número de la sección actual

    // Iteramos sobre cada línea del archivo.
    for await (const linea of rl) {
        
        if (/^\d+$/.test(linea)) { // Verifica si la línea es un número de sección
            //Mostrar el número de la 
            
            if (seccionActual) {
                // console.log(id);
                // Si ya hay una sección acumulada, la procesamos.
                const resultado = convertirTextoAJson(seccionActual);
                lista_mat_simulador.push(resultado); // Agrega el objeto JSON al arreglo
            }
            // Reiniciamos la sección actual para la nueva sección.
            seccionActual = "";
            seccionNumero = linea;
        } else {
            // Agregamos la línea a la sección actual.
            seccionActual += linea + "\n";
        }
    }

    // Procesamos la última sección después de terminar el bucle.
    if (seccionActual) {
        const resultado = convertirTextoAJson(seccionActual);
        lista_mat_simulador.push(resultado); // Agrega el objeto JSON al arreglo
    }
    
}

// Llamamos a procesarArchivo usando await en una función async
const main = async() =>{
    await procesarArchivo("Simuladores_Links.txt");
    
    // Convertir la lista en una cadena JSON formateada
    const jsonContent = JSON.stringify(lista_mat_simulador, null, 2);
    // for(let i = 0; i<jsonContent.length;i++){
    //     if(jsonContent[i].json['Nivel']){
    //         console.log(jsonContent[i]);
    //     }
    // }
    // Escribir la cadena JSON en un archivo
    fs.writeFileSync('salida.json', jsonContent, 'utf8');
}

main();

