var myTheme = {
  init: function () {
    var ie_v = $exe.isIE();
    if (ie_v && ie_v < 8) return false;
    setTimeout(function () {
      $(window).resize(function () {
        myTheme.reset();
      });
    }, 1000);
    //
    var home =
      $(`<span style="position:absolute; top:.65rem; left:.7rem;font-weight:800">
		<a href="https://${window.location.hostname}/home"> 
			<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-house" viewBox="0 0 16 16">
				<path d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.708L2 8.207V13.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V8.207l.646.647a.5.5 0 0 0 .708-.708L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293zM13 7.207V13.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V7.207l5-5z"/>
			</svg>
		</a>
		</span>`);
    var l = $(
      '<p id="nav-toggler"><a href="#" onclick="myTheme.toggleMenu(this);return false" class="hide-nav" id="toggle-nav" title="' +
        $exe_i18n.hide +
        '"><span>' +
        $exe_i18n.menu +
        "</span></a></p>"
    );
    $("#siteNav").before(home);
    $("#siteNav").before(l);
    $("#topPagination .pagination").prepend(
      '<a href="#" onclick="window.print();return false" title="' +
        $exe_i18n.print +
        '" class="print-page"><span>' +
        $exe_i18n.print +
        "</span></a> "
    );
    this.addNavArrows();
    this.bigInputs();
    var url = window.location.href;
    url = url.split("?");
    if (url.length > 1) {
      if (url[1].indexOf("nav=false") != -1) {
        myTheme.hideMenu();
        return false;
      }
    }
    myTheme.setNavHeight();
    // We execute this more than once because sometimes the height changes because of the videos, etc.
    setTimeout(function () {
      myTheme.setNavHeight();
    }, 1000);
    $(window).load(function () {
      myTheme.setNavHeight();
    });
  },
  isMobile: function () {
    try {
      document.createEvent("TouchEvent");
      return true;
    } catch (e) {
      return false;
    }
  },
  bigInputs: function () {
    if (this.isMobile()) {
      $(
        ".MultiSelectIdevice,.MultichoiceIdevice,.QuizTestIdevice,.TrueFalseIdevice"
      ).each(function () {
        $("input:radio", this).screwDefaultButtons({
          image: 'url("_style_input_radio.png")',
          width: 30,
          height: 30,
        });
        $("input:checkbox", this).screwDefaultButtons({
          image: 'url("_style_input_checkbox.png")',
          width: 30,
          height: 30,
        });
        $(this).addClass("custom-inputs");
      });
    }
  },
  addNavArrows: function () {
    $("#siteNav ul ul .daddy").each(function () {
      this.innerHTML += "<span> &#9658;</span>";
    });
  },
  hideMenu: function () {
    $("#siteNav").hide();
    $(document.body).addClass("no-nav");
    myTheme.params("add");
    $("#toggle-nav").attr("class", "show-nav").attr("title", $exe_i18n.show);
  },
  setNavHeight: function () {
    var n = $("#siteNav");
    var c = $("#main-wrapper");
    var nH = n.height();
    var cH = c.height();
    var isMobile = $("#siteNav").css("float") == "none";
    if (cH < nH) {
      cH = nH;
      if (!isMobile) {
        var s = c.attr("style");
        if (s) c.css("min-height", cH + "px");
        else
          c.attr(
            "style",
            "height:auto!important;height:" + cH + "px;min-height:" + cH + "px"
          );
      }
    }
    var h = cH - nH + 24 + "px";
    var m = 0;
    if (isMobile) {
      h = 0;
      m = "15px";
    } else if (n.css("display") == "table") {
      h = 0;
    }
    n.css({
      "padding-bottom": h,
      "margin-bottom": m,
    });
  },
  toggleMenu: function (e) {
    if (typeof myTheme.isToggling == "undefined") myTheme.isToggling = false;
    if (myTheme.isToggling) return false;

    var l = $("#toggle-nav");

    if (!e && $(window).width() < 900 && l.css("display") != "none")
      return false; // No reset in mobile view
    if (!e) l.attr("class", "show-nav").attr("title", $exe_i18n.show); // Reset

    myTheme.isToggling = true;

    if (l.attr("class") == "hide-nav") {
      l.attr("class", "show-nav").attr("title", $exe_i18n.show);
      $("#siteFooter").hide();
      $("#siteNav").slideUp(400, function () {
        $(document.body).addClass("no-nav");
        $("#siteFooter").show();
        myTheme.isToggling = false;
      });
      myTheme.params("add");
    } else {
      l.attr("class", "hide-nav").attr("title", $exe_i18n.hide);
      $(document.body).removeClass("no-nav");
      $("#siteNav").slideDown(400, function () {
        myTheme.isToggling = false;
        myTheme.setNavHeight();
      });
      myTheme.params("delete");
    }
  },
  param: function (e, act) {
    if (act == "add") {
      var ref = e.href;
      var con = "?";
      if (ref.indexOf(".html?") != -1 || ref.indexOf(".htm?") != -1) con = "&";
      var param = "nav=false";
      if (ref.indexOf(param) == -1) {
        ref += con + param;
        e.href = ref;
      }
    } else {
      // This will remove all params
      var ref = e.href;
      ref = ref.split("?");
      e.href = ref[0];
    }
  },
  params: function (act) {
    $("A", ".pagination").each(function () {
      myTheme.param(this, act);
    });
  },
  reset: function () {
    myTheme.toggleMenu();
    myTheme.setNavHeight();
  },
  getCustomIcons: function () {
    // Provisional solution so the user can use the iDevice Editor to choose an icon
    $(".iDevice_header").each(function () {
      var i = this.style.backgroundImage;
      if (i != "") $(".iDeviceTitle", this).css("background-image", i);
      this.style.backgroundImage = "none";
    });
  },
};
$(function () {
  if ($("body").hasClass("exe-web-site")) {
    myTheme.init();
  }
  myTheme.getCustomIcons();
});

const agregarBanner = () => {
	// Crear el nuevo div
	var nuevoDiv = document.createElement("div");
	nuevoDiv.id = "img_banner";
  
	// Obtener el elemento con id "header"
	var header = document.getElementById("header");
  
	// Mover el header dentro del nuevo div
	header.parentNode.insertBefore(nuevoDiv, header);
	nuevoDiv.appendChild(header);
  };
document.addEventListener("DOMContentLoaded", ()=>agregarBanner());

const nodeTitle = document.getElementById("nodeTitle");


// Función para manejar el cambio de tamaño del elemento "nodeTitle"
function handleNodeTitleResize() {
  const article = document.querySelector("article");
  const nodeTitleHeight = nodeTitle.offsetHeight; // Obtener la altura del elemento en px

  // Calcular el margin-top deseado (4rem + altura del elemento)
  const marginTopInRem = 4 + nodeTitleHeight / 16 - 3; // Suponiendo que 1rem = 16px

  // Verificar si el tamaño de fuente es mayor a 41px
  if (nodeTitleHeight > 41) {
    // Aplicar el nuevo margin-top en rem
    article.style.marginTop = `${marginTopInRem}rem`;
  } else {
    // Restablecer el margin-top si el tamaño de fuente es menor o igual a 41px
    article.style.marginTop = "4rem";
  }
}

// Función que se ejecutará cuando se detecten cambios en el elemento "siteNav"
function handleMutations(mutationsList) {
  const siteNav = document.getElementById("siteNav");
  const nodeDecoration = document.getElementById("nodeDecoration");
  const header = document.getElementById("header");

  for (let mutation of mutationsList) {
    // Verifica si el atributo "style" ha cambiado
    if (
      mutation.attributeName === "style" ||
      (mutation.attributeName === "orientation" &&
        mutation.type === "attributes")
    ) {
      const isPortrait = window.matchMedia("(orientation: portrait").matches;
      const isSiteNavHidden =
        window.getComputedStyle(siteNav).display === "none";
      const valor = window.getComputedStyle(nodeDecoration);
      // const siteNav = mutation.target;
      // Verifica si el valor de "display" es "none"
      if (isSiteNavHidden) {
        if (isPortrait) {
          const screenHeight = screen.height;
          const headerStyles = window.getComputedStyle(header);
          const headerHeight = parseFloat(headerStyles.height);
          const calculatedTop = headerHeight + 20;
          //siteNav inactivo, orientación vertical
          // Si es "none", modifica el valor de "left" del elemento "header" con id "nodeDecoration"
          nodeDecoration.style.left = "1rem";
          nodeDecoration.style.top = `${calculatedTop}px`;
          
        } else {
          const screenHeight = screen.height;
          const headerStyles = window.getComputedStyle(header);
          const headerHeight = parseFloat(headerStyles.height);
          const calculatedTop = headerHeight + 20;
          //siteNav inactivo y orientación horizontal
          nodeDecoration.style.left = "1rem";
          nodeDecoration.style.top = `${calculatedTop}px`;
        }

        // const nodeDecoration = document.getElementById('nodeDecoration');
        // nodeDecoration.style.left = '1rem';
      } else {
        if (isPortrait) {
          const headerStyles = window.getComputedStyle(header);
          const headerHeight = parseFloat(headerStyles.height);
          const calculatedTop = headerHeight + 20;
          // siteNav activo, orientación vertical
          nodeDecoration.style.left = "calc(16rem + var(--tamano-body)*0.06)";
          nodeDecoration.style.top = `${calculatedTop}px`;
        } else {
          const headerStyles = window.getComputedStyle(header);
          const headerHeight = parseFloat(headerStyles.height);
          const calculatedTop = headerHeight + 20;
          // siteNav activo, orientación horizontal
          nodeDecoration.style.left = "calc(16rem + var(--tamano-body)*0.06)";
          nodeDecoration.style.top = `${calculatedTop}px`;
        }
        // const nodeDecoration = document.getElementById('nodeDecoration');
      }
    }
  }
}

// Selecciona el elemento con id "siteNav"
const siteNav = document.getElementById("siteNav");

// Crea una nueva instancia de MutationObserver
const observer = new MutationObserver(handleMutations);

// Configura el observer para escuchar cambios en el atributo "style"
observer.observe(siteNav, { attributes: true, attributeFilter: ["style"] });

// Agregar un evento de cambio de tamaño a window para manejar el cambio de tamaño de nodeTitle
window.addEventListener("resize", handleNodeTitleResize());

/* PINTAR EL HEADER DE COLOR BLANCO CUANDO SE HAGA UN SCROLL HACIA ABAJO */

function handleScroll() {
  const nodeDecoration = document.getElementById("nodeDecoration");
  const targetScroll = 10;

  if (document.getElementById("main").scrollTop >= targetScroll) {
    nodeDecoration.style.background = "white";
  } else {
    nodeDecoration.style.background = "none";
  }
}

document.getElementById("main").addEventListener("scroll", handleScroll);

/**
 * Scroll Persistence for #siteNav
 *
 * Este script permite que, al seleccionar un enlace dentro de #siteNav,
 * la posición de desplazamiento se guarde en localStorage. Al recargar
 * la página, el nav se desplazará automáticamente a la posición guardada,
 * mostrando el enlace seleccionado en primer lugar.
 */
document.addEventListener("DOMContentLoaded", function () {
  // Obtener el elemento siteNav
  var siteNav = document.getElementById("siteNav");

  // Si existe siteNav y localStorage tiene la posición guardada, desplázate a esa posición
  if (siteNav && localStorage.getItem("scrollPosition")) {
    siteNav.scrollTop = localStorage.getItem("scrollPosition");
  }

  // Agregar un evento de clic a todos los enlaces dentro de siteNav
  var navLinks = siteNav.querySelectorAll("a");
  navLinks.forEach(function (link) {
    link.addEventListener("click", function () {
      // Al hacer clic en un enlace, almacena la posición de desplazamiento actual en localStorage
      localStorage.setItem("scrollPosition", siteNav.scrollTop);
    });
  });
});

/* En dispositivos móbiles el #PackageLicense solo se muestra 
si el valor del margin-bottom es de 10.5em en orientación horizontal
 y se muestra a partir de 8.5em en orientacion vertical */

/* La funcion handleLicense debe cambiar dicho valor dependiendo de la orientación del dispositivo */
function handleLicense() {
  const packageLicense = document.getElementById("packageLicense");

  const setMarginBottom = () => {
    const isPortrait = window.matchMedia("(orientation: portrait)").matches;
    if (isPortrait) {
      //Si es vertical marginbottom es 8.5em
      packageLicense.style.marginBottom = "8.5em";
    } else {
      //Si es horizontal marginbottom es 10.5em
      packageLicense.style.marginBottom = "10.5em";
    }
  };

  setMarginBottom();

  window.addEventListener("resize", setMarginBottom);
  window.addEventListener("orientationchange", setMarginBottom);
}

handleLicense(); // Llamada inicial para establecer el margen de acuerdo a la orientación inicial

// FORMATO PARA LAS TABLAS

// Selecciona todas las tablas en el documento
const tables = document.querySelectorAll("table");

const pintarHeaderTable = (tablas) => {
  tablas.forEach((tabla) => {
    // Aplica el estilo a la primera fila de cada tabla
    const firstRow = tabla.querySelector("tr:first-child");
    if (firstRow) {
      firstRow.style.color = "#D5DDE5";
      firstRow.style.color = "#D5DDE5";
      firstRow.style.background = "#263238";
      firstRow.style.borderBottom = "4px solid var(--color-header-tablas)";
      firstRow.style.borderRight = "1px solid #343a45";
      firstRow.style.borderTop = "0px";
      firstRow.style.borderLeft = "0px";
      firstRow.style.fontWeight = "100";
      firstRow.style.padding = ".6rem";
      firstRow.style.textAlign = "left";
      firstRow.style.textShadow = "0 1px 1px rgba(0, 0, 0, 0.1)";
      firstRow.style.verticalAlign = "middle";
      firstRow.querySelectorAll("th, td").forEach((cell) => {
        cell.style.background = "#263238";
        cell.style.borderBottom = "4px solid var(--color-header-tablas)";
        cell.style.borderRight = "1px solid #343a45";
        cell.style.borderTop = "0px";
        cell.style.borderLeft = "0px";
        cell.style.fontWeight = "100";
        cell.style.padding = ".6rem";
        cell.style.textAlign = "left";
        cell.style.textShadow = "0 1px 1px rgba(0, 0, 0, 0.1)";
        cell.style.verticalAlign = "middle";
      });
    }
  });
};

pintarHeaderTable(tables);

/*!
 * ScrewDefaultButtons v2.0.6
 * http://screwdefaultbuttons.com/
 *
 * Licensed under the MIT license.
 * Copyright 2013 Matt Solano http://mattsolano.com
 *
 * Date: Mon February 25 2013
 */ (function (e, t, n, r) {
  var i = {
    init: function (t) {
      var n = e.extend({ image: null, width: 50, height: 50, disabled: !1 }, t);
      return this.each(function () {
        var t = e(this),
          r = n.image,
          i = t.data("sdb-image");
        i && (r = i);
        r || e.error("There is no image assigned for ScrewDefaultButtons");
        t.wrap("<div >").css({ display: "none" });
        var s = t.attr("class"),
          o = t.attr("onclick"),
          u = t.parent("div");
        u.addClass(s);
        u.attr("onclick", o);
        u.css({
          "background-image": r,
          width: n.width,
          height: n.height,
          cursor: "pointer",
        });
        var a = 0,
          f = -n.height;
        if (t.is(":disabled")) {
          a = -(n.height * 2);
          f = -(n.height * 3);
        }
        t.on("disableBtn", function () {
          t.attr("disabled", "disabled");
          a = -(n.height * 2);
          f = -(n.height * 3);
          t.trigger("resetBackground");
        });
        t.on("enableBtn", function () {
          t.removeAttr("disabled");
          a = 0;
          f = -n.height;
          t.trigger("resetBackground");
        });
        t.on("resetBackground", function () {
          t.is(":checked")
            ? u.css({ backgroundPosition: "0 " + f + "px" })
            : u.css({ backgroundPosition: "0 " + a + "px" });
        });
        t.trigger("resetBackground");
        if (t.is(":checkbox")) {
          u.on("click", function () {
            t.is(":disabled") || t.change();
          });
          u.addClass("styledCheckbox");
          t.on("change", function () {
            if (t.prop("checked")) {
              t.prop("checked", !1);
              u.css({ backgroundPosition: "0 " + a + "px" });
            } else {
              t.prop("checked", !0);
              u.css({ backgroundPosition: "0 " + f + "px" });
            }
          });
        } else if (t.is(":radio")) {
          u.addClass("styledRadio");
          var l = t.attr("name");
          u.on("click", function () {
            !t.prop("checked") && !t.is(":disabled") && t.change();
          });
          t.on("change", function () {
            if (t.prop("checked")) {
              t.prop("checked", !1);
              u.css({ backgroundPosition: "0 " + a + "px" });
            } else {
              t.prop("checked", !0);
              u.css({ backgroundPosition: "0 " + f + "px" });
              var n = e('input[name="' + l + '"]').not(t);
              n.trigger("radioSwitch");
            }
          });
          t.on("radioSwitch", function () {
            u.css({ backgroundPosition: "0 " + a + "px" });
          });
          var c = e(this).attr("id"),
            h = e('label[for="' + c + '"]');
          h.on("click", function () {
            u.trigger("click");
          });
        }
        if (!e.support.leadingWhitespace) {
          var c = e(this).attr("id"),
            h = e('label[for="' + c + '"]');
          h.on("click", function () {
            u.trigger("click");
          });
        }
      });
    },
    check: function () {
      return this.each(function () {
        var t = e(this);
        i.isChecked(t) || t.change();
      });
    },
    uncheck: function () {
      return this.each(function () {
        var t = e(this);
        i.isChecked(t) && t.change();
      });
    },
    toggle: function () {
      return this.each(function () {
        var t = e(this);
        t.change();
      });
    },
    disable: function () {
      return this.each(function () {
        var t = e(this);
        t.trigger("disableBtn");
      });
    },
    enable: function () {
      return this.each(function () {
        var t = e(this);
        t.trigger("enableBtn");
      });
    },
    isChecked: function (e) {
      return e.prop("checked") ? !0 : !1;
    },
  };
  e.fn.screwDefaultButtons = function (t, n) {
    if (i[t]) return i[t].apply(this, Array.prototype.slice.call(arguments, 1));
    if (typeof t == "object" || !t) return i.init.apply(this, arguments);
    e.error("Method " + t + " does not exist on jQuery.screwDefaultButtons");
  };
  return this;
})(jQuery);
