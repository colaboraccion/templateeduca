var myTheme = {
	init : function(){
		var ie_v = $exe.isIE();
		if (ie_v && ie_v<8) return false;
		setTimeout(function(){
			$(window).resize(function() {
				myTheme.reset();
			});
		},1000);
		var l = $('<p id="nav-toggler"><a href="#" onclick="myTheme.toggleMenu(this);return false" class="hide-nav" id="toggle-nav" title="'+$exe_i18n.hide+'"><span>'+$exe_i18n.menu+'</span></a></p>');
		$("#siteNav").before(l);
		$("#topPagination .pagination").prepend('<a href="#" onclick="window.print();return false" title="'+$exe_i18n.print+'" class="print-page"><span>'+$exe_i18n.print+'</span></a> ');
		this.addNavArrows();
		this.bigInputs();		
		var url = window.location.href;
		url = url.split("?");
		if (url.length>1){
			if (url[1].indexOf("nav=false")!=-1) {
				myTheme.hideMenu();
				return false;
			}
		}
		myTheme.setNavHeight();
		// We execute this more than once because sometimes the height changes because of the videos, etc.
		setTimeout(function(){
			myTheme.setNavHeight();
		},1000);
		$(window).load(function(){
			myTheme.setNavHeight();
		});
	},
	isMobile : function(){
		try {
			document.createEvent("TouchEvent");
			return true; 
		} catch(e) {
			return false;
		}
	},	
	bigInputs : function(){
		if (this.isMobile()) {
			$(".MultiSelectIdevice,.MultichoiceIdevice,.QuizTestIdevice,.TrueFalseIdevice").each(function(){
				$('input:radio',this).screwDefaultButtons({
					image: 'url("_style_input_radio.png")',
					width: 30,
					height: 30
				});
				$('input:checkbox',this).screwDefaultButtons({
					image: 'url("_style_input_checkbox.png")',
					width: 30,
					height: 30
				});
				$(this).addClass("custom-inputs");
			});
		}		
	},
	addNavArrows : function(){
		$("#siteNav ul ul .daddy").each(
			function(){
				this.innerHTML+='<span> &#9658;</span>';
			}
		);
	},	
	hideMenu : function(){
		$("#siteNav").hide();
		$(document.body).addClass("no-nav");
		myTheme.params("add");
		$("#toggle-nav").attr("class","show-nav").attr("title",$exe_i18n.show);
	},
	setNavHeight : function(){
		var n = $("#siteNav");
		var c = $("#main-wrapper");
		var nH = n.height();
		var cH = c.height();
		var isMobile = $("#siteNav").css("float")=="none";
		if (cH<nH) {
			cH = nH;
			if (!isMobile) {
				var s = c.attr("style");
				if (s) c.css("min-height",cH+"px");
				else c.attr("style","height:auto!important;height:"+cH+"px;min-height:"+cH+"px");
			}
		}
		var h = (cH-nH+24)+"px";
		var m = 0;
		if (isMobile) {
			h = 0;
			m = "15px";
		} else if (n.css("display")=="table") {
			h = 0;
		}
		n.css({
			"padding-bottom":h,
			"margin-bottom":m
		});
	},
	toggleMenu : function(e){
		if (typeof(myTheme.isToggling)=='undefined') myTheme.isToggling = false;
		if (myTheme.isToggling) return false;
		
		var l = $("#toggle-nav");
		
		if (!e && $(window).width()<900 && l.css("display")!='none') return false; // No reset in mobile view
		if (!e) l.attr("class","show-nav").attr("title",$exe_i18n.show); // Reset
		
		myTheme.isToggling = true;
		
		if (l.attr("class")=='hide-nav') {       
			l.attr("class","show-nav").attr("title",$exe_i18n.show);
			$("#siteFooter").hide();
			$("#siteNav").slideUp(400,function(){
				$(document.body).addClass("no-nav");
				$("#siteFooter").show();
				myTheme.isToggling = false;
			}); 
			myTheme.params("add");
		} else {
			l.attr("class","hide-nav").attr("title",$exe_i18n.hide);
			$(document.body).removeClass("no-nav");
			$("#siteNav").slideDown(400,function(){
				myTheme.isToggling = false;
				myTheme.setNavHeight();
			});
			myTheme.params("delete");            
		}
		
	},
	param : function(e,act) {
		if (act=="add") {
			var ref = e.href;
			var con = "?";
			if (ref.indexOf(".html?")!=-1 || ref.indexOf(".htm?")!=-1) con = "&";
			var param = "nav=false";
			if (ref.indexOf(param)==-1) {
				ref += con+param;
				e.href = ref;                    
			}            
		} else {
			// This will remove all params
			var ref = e.href;
			ref = ref.split("?");
			e.href = ref[0];
		}
	},
	params : function(act){
		$("A",".pagination").each(function(){
			myTheme.param(this,act);
		});
	},
	reset : function() {
		myTheme.toggleMenu();        
		myTheme.setNavHeight();
	},
	getCustomIcons : function(){
		// Provisional solution so the user can use the iDevice Editor to choose an icon
		$(".iDevice_header").each(function(){
			var i = this.style.backgroundImage;
			if (i!="") $(".iDeviceTitle",this).css("background-image",i);
			this.style.backgroundImage = "none";
		});
	}
}
$(function(){
	if ($("body").hasClass("exe-web-site")) {
		myTheme.init();
	}
	myTheme.getCustomIcons();
});

let jsonData = [];

let jsonActual=jsonData;
const resultadosPorPagina = 10;
let paginaActual = 1;
let palabraBuscada=null;
  
const crear_buscador= () => document.addEventListener('DOMContentLoaded', function() {
	// Encuentra la tabla en el documento
	const table = document.querySelector('table');
	table.id="tablaMaster";
	
	// Crea el span contenedor para el encabezado y los botones
	const spanContainer = document.createElement('span');
	spanContainer.style.display = 'flex';
	spanContainer.style.alignItems = 'center';
	spanContainer.style.justifyContent = 'space-between';
	
	// Mueve el título existente al nuevo span contenedor
	const strong = table.querySelector('strong');
	spanContainer.appendChild(strong);
	
	// Crea el span para los elementos de entrada
	const inputGroup = document.createElement('span');
	inputGroup.style.display = 'flex';
	
	// Crea el campo de entrada de texto
	const input = document.createElement('input');
	input.id="input_text"
	input.type = 'text';
	input.style.textAlign = 'center';
	input.style.width = '10rem';
	input.style.flexGrow = '1';
	input.style.border = 'none';
	input.style.borderRadius = '5px';
	input.style.marginRight = '10px';
	input.placeholder = 'Escribe una palabra';
	
	// Crea el botón de búsqueda
	const searchButton = document.createElement('button');
	searchButton.id="searchButton";
	searchButton.textContent = 'Buscar';
	searchButton.style.border = 'none';
	searchButton.style.color = 'white';
	searchButton.style.backgroundColor = '#4b779f';
	searchButton.style.padding = '10px 20px';
	searchButton.style.borderRadius = '5px';
	searchButton.style.cursor = 'pointer';
	searchButton.style.marginRight = '10px';
	
	// Crea el botón de borrar
	const clearButton = document.createElement('button');
	clearButton.id="clearButton";
	clearButton.textContent = 'Borrar';
	clearButton.style.border = 'none';
	clearButton.style.color = 'white';
	clearButton.style.backgroundColor = '#9f1d1d';
	clearButton.style.padding = '10px 20px';
	clearButton.style.borderRadius = '5px';
	clearButton.style.cursor = 'pointer';
	
	// Agrega los elementos de entrada y los botones al span de grupo de entrada
	inputGroup.appendChild(input);
	inputGroup.appendChild(searchButton);
	inputGroup.appendChild(clearButton);
	
	// Agrega el span de grupo de entrada al span contenedor
	spanContainer.appendChild(inputGroup);
	
	// Encuentra la celda de la tabla y limpia su contenido
	const cell = table.querySelector('td');
	cell.innerHTML = '';
	
	// Agrega el span contenedor a la celda de la tabla
	cell.appendChild(spanContainer);
  });
 crear_buscador();

/////// FORMATO PARA LAS TABLAS /////////
 const pintarHeaderTable = (tablas) => {
    tablas.forEach(tabla => {
        // Aplica el estilo a la primera fila de cada tabla
        const firstRow = tabla.querySelector('tr:first-child');
        if (firstRow) {
            firstRow.style.color = "#D5DDE5";
			firstRow.style.color = "#D5DDE5";
			firstRow.style.background = "#263238";
			firstRow.style.borderBottom = "4px solid var(--color-header-tablas)";
			firstRow.style.borderRight = "1px solid #343a45";
			firstRow.style.borderTop = "0px";
			firstRow.style.borderLeft = "0px";
			firstRow.style.fontWeight = "100";
			firstRow.style.padding = ".6rem";
			firstRow.style.textAlign = "left";
			firstRow.style.textShadow = "0 1px 1px rgba(0, 0, 0, 0.1)";
			firstRow.style.verticalAlign = "middle";
            firstRow.querySelectorAll('th, td').forEach(cell => {
                cell.style.background = "#263238";
				cell.style.borderBottom = "4px solid var(--color-header-tablas)";
				cell.style.borderRight = "1px solid #343a45";
				cell.style.borderTop = "0px";
				cell.style.borderLeft = "0px";
				cell.style.fontWeight = "100";
				cell.style.padding = ".6rem";
				cell.style.textAlign = "left";
				cell.style.textShadow = "0 1px 1px rgba(0, 0, 0, 0.1)";
				cell.style.verticalAlign = "middle";
            });
        }
    });
}

const capitalizarPrimeraLetra=(str)=> (str.charAt(0).toUpperCase() + str.slice(1).toLowerCase());

// Selecciona todas las tablas en el documento
const tables = document.querySelectorAll('table');
pintarHeaderTable(tables);

const resaltarPalabra = (texto, palabra) => {
	if (!palabra) return texto;
	// Escapa caracteres especiales para evitar problemas con expresiones regulares
	const terminoSeguro = palabra.replace(/[-\/\\^$*+?.()|[\]{}<>]/g, '\\$&');
	const regex = new RegExp(`(${terminoSeguro})`, 'gi');

	return texto?.replace(regex, '<span class="resaltado">$1</span>');
};

////// CREACIÓN Y DISTRIBUCIÓN DE LOS DATOS DEL JS EN UNA TABLA
const renderizarTabla= (data, terminoBusqueda=null)=>{
	const tableBody = document.querySelector('#tablaMaster tbody');

	// Elimina todas las filas, excepto la del encabezado
	let rows = tableBody.querySelectorAll('tr:not(:first-child)');

	rows.forEach(row => row.remove());

	if(data.length===0){
		// Crea una nueva fila con un mensaje de que no se encontraron resultados
        const row = document.createElement('tr');
        const cell = document.createElement('td');
        // Asegúrate de que el mensaje abarque todas las columnas que tiene la tabla
        cell.setAttribute('colspan', tableBody.closest('table').querySelectorAll('th').length);
        cell.style.textAlign = 'center'; // Centra el texto en la celda
        cell.textContent = `No se encontraron resultados para ${terminoBusqueda}`;
        row.appendChild(cell);
        tableBody.appendChild(row);
	}else{
			
		// Ahora, agrega los nuevos datos
		data.forEach(item => {
			
			const row = document.createElement('tr');
			const idCell = document.createElement('td');
			idCell.textContent = item.id;
			idCell.stylewidth = `20px`;
			idCell.innerHTML = terminoBusqueda ? resaltarPalabra(idCell.textContent,terminoBusqueda) : idCell.textContent;
			row.appendChild(idCell);

			const infoCell = document.createElement('td');

			for (const key in item) {
				if (key !== "Referencia Pedagógica" && key!=="Referencia Temática" && key !== 'id' && item[key] !== " ") {
					const p = document.createElement('p');
					if(key==="Area"){
						item[key]=capitalizarPrimeraLetra(item[key]); 	
						// Resaltar la palabra buscada en el texto
						p.innerHTML = terminoBusqueda ? `Área: ${resaltarPalabra(item[key], terminoBusqueda)}` : `Área: ${item[key]}`;
						
					}
					else if(key==="SubArea"){
						p.innerHTML = terminoBusqueda ? `Sub Área: ${resaltarPalabra(item[key], terminoBusqueda)}` : `Sub Área: ${item[key]}`;
					}
					else{
						p.innerHTML = terminoBusqueda ? `${key}: ${resaltarPalabra(item[key], terminoBusqueda)}` : `${key}: ${item[key]}`;
					}
					infoCell.appendChild(p);
				  }
				else if(key ==="Referencia Pedagógica"){
					let TieneServer= false;
					(item["Referencia Pedagógica"]["ServerEscuela"].trim()!=="") ? TieneServer=true : TieneServer=false;
					for(const subkey in item["Referencia Pedagógica"]){
						if((item["Referencia Pedagógica"][subkey]).trim()!==""){
							if (subkey==="ServerEscuela" && TieneServer) {
								const a=document.createElement('a');
								const path=`${window.location.protocol}//${window.location.hostname}/${item["Referencia Pedagógica"][subkey]}`
								a.textContent=path;
								a.href=path;
								a.style="text-decoration:none";
								a.target='_blank';

								const span= document.createElement('span');
								span.innerHTML = `URL: `
								span.appendChild(a);
								infoCell.appendChild(span);	

							}
							else if(subkey==="URL" && !TieneServer)
							{
								const valor=item["Referencia Pedagógica"][subkey]
								const a=document.createElement('a');
								a.textContent=valor
								a.style="text-decoration:none";
								a.href=valor;

								const span= document.createElement('span');
								span.innerHTML = `URL: `
								span.appendChild(a);
					
								infoCell.appendChild(span);
							}
							else if(subkey==="Introduccion"){ //Introducción
								const p = document.createElement('p');
								const textoOriginal = item["Referencia Pedagógica"][subkey];
								// Resaltar la palabra buscada en el texto
								p.innerHTML = terminoBusqueda ? `Introducción: ${resaltarPalabra(textoOriginal, terminoBusqueda)}` : `Introducción: ${textoOriginal}`;
								infoCell.appendChild(p);
							}
						}
					}
				}
				else if(key==="Referencia Temática"){
					if(item[key].trim()!==""){

						const div= document.createElement('div');
						div.textContent=`${key}:`;
						let table = document.createElement('table');
						table.id=`minitabla${item["id"]}`
						table.className = "minitable";
						let tbody = document.createElement('tbody');
						let tr= document.createElement('tr');
						tr.textContent = "Referencia Temática";
						let td=document.createElement('td');
						let trNivel= document.createElement('tr');
						trNivel.appendChild(td);
						tbody.appendChild(trNivel);
						const texto = item[key];
						const regex = /Nivel\s*\d+\s*Lección\s*\d+:.*?(?=(Nivel\s*\d+\s*Lección\s*\d+:|$))/gs;
						let coincidencias;
						const secciones = [];
						while ((coincidencias = regex.exec(texto)) !== null) {
							secciones.push(coincidencias[0]);
						}
	
						const listaNiveles = []; // Este array almacenará los niveles y lecciones
						for(let seccion of secciones){
							const regexNivel = /Nivel\s*\d+/;
							const regexLeccion = /Lección\s*\d+/;
							const regexContenido = /: (.*)|: (.*)/; // Para capturar el contenido después de "Lección x:"
						
							const nivelMatch = seccion.match(regexNivel)[0]; // Obtiene el match de "Nivel x"
							const leccionMatch = seccion.match(regexLeccion)[0]; // Obtiene el match de "Lección x"
							
							const contenidoMatch = seccion.match(regexContenido)[1]; // Obtiene el contenido después de "Lección x:"
							
							// Verifica si el nivel ya existe
							let nivelIndex = listaNiveles.findIndex(nivel => nivel[0] === nivelMatch);
							if (nivelIndex === -1) {
								// Si el nivel no existe, lo añade con un array vacío para las lecciones
								nivelIndex = listaNiveles.length;
								listaNiveles.push([nivelMatch, []]);
							}
						
							// Añade la lección y su contenido al nivel correspondiente
							listaNiveles[nivelIndex][1].push([leccionMatch, contenidoMatch]);
						}
						const nuevaLeccion=[];
	
						for( let nivel of listaNiveles ) {
							const tdNivel=document.createElement("td");
							tdNivel.textContent =`Nivel ${nivel[0].match(/\d+/g)}`;
	
							for(let leccion of nivel[1]) {
								const tdContenido= document.createElement("td");
								if(!nuevaLeccion.includes(leccion[0])){
									
									let trLeccion= document.createElement("tr");
									const textoAjustado = leccion[0].replace(/(Lección)(\d+)/, "$1_$2");
									trLeccion.className= textoAjustado.replace(/\s+/g, '_');
									const tdLeccion= document.createElement("td");
									tdLeccion.textContent= `Lección ${leccion[0].match(/\d+/g)}`;
									tdLeccion.style.width=`70px`;
									trLeccion.appendChild(tdLeccion);
									const numeroNivel =Number(nivel[0].match(/\d+/g));

									if(numeroNivel===1){
										tdContenido.innerHTML = terminoBusqueda ? resaltarPalabra(leccion[1]?.trim(), terminoBusqueda) : leccion[1];
										trLeccion.appendChild(tdContenido);	
									}
									else{
										for(let contador=1; contador<= numeroNivel; contador++){
											if(contador===numeroNivel){
												tdContenido.innerHTML = terminoBusqueda ? resaltarPalabra(leccion[1].trim(), terminoBusqueda) : leccion[1].trim();
												trLeccion.appendChild(tdContenido);	
											}		
											else{
												const tdContenidoVacio =document.createElement("td");
												tdContenidoVacio.innerHTML="";
												trLeccion.appendChild(tdContenidoVacio);	
											}						
	
										}
									}
									
									tbody.appendChild(trLeccion);	
									nuevaLeccion.push(leccion[0])
								}
								else{
									
									const textoAjustado= leccion[0].replace(/(Lección)(\d+)/, "$1_$2")
									const palabra_buscada=textoAjustado.replace(/\s+/g, '_');
									const trLeccion=tbody.querySelector(`.${palabra_buscada}`);
									const numeroNivel =Number(nivel[0].match(/\d+/g));
									
									for(let contador=1; contador<=numeroNivel; contador++){
										const contenidoCelda =trLeccion.cells[contador];
										if(contador===numeroNivel){
											tdContenido.innerHTML = terminoBusqueda ? resaltarPalabra(leccion[1].trim(), terminoBusqueda) : leccion[1].trim();
											trLeccion.appendChild(tdContenido);
										}								
										else{
											if(contenidoCelda===undefined){												
												tdContenido.innerHTML="";
												trLeccion.appendChild(tdContenido);
											}
										}
									}
								}
							}
							trNivel.appendChild(tdNivel);
						}
	
						table.appendChild(tbody);
						infoCell.appendChild(div);
						infoCell.appendChild(table);
						
					}
				  }
			}
			row.appendChild(infoCell);
			tableBody.appendChild(row);
		});

	}
    
    // Luego de crear las mini tablas, aplica los estilos de encabezado
    pintarHeaderTable(document.querySelectorAll('.minitable'));
};

// Función para actualizar el estado de los botones de paginación
const actualizarEstadoBotones = (paginaActual, maxPaginas) => {
	const botonAnterior = document.getElementById('anterior');
	const botonSiguiente = document.getElementById('siguiente');
  
	if (botonAnterior) {
	  botonAnterior.classList.toggle('disabled', paginaActual <= 1);
	  botonAnterior.disabled = paginaActual <= 1; // Deshabilita el botón si estamos en la primera página
	}
  
	if (botonSiguiente) {
	  botonSiguiente.classList.toggle('disabled', paginaActual >= maxPaginas);
	  botonSiguiente.disabled = paginaActual >= maxPaginas; // Deshabilita el botón si estamos en la última página
	}
  };

// Función para cambiar la página
const cambiarPagina = (pagina, jsonActual, terminoBusqueda=null) => {
	let inicio = (jsonActual.length >=1) ? ((pagina - 1) * resultadosPorPagina) : 1; //Si la página tiene más de 1 elemento se irá actualizando, sino, comenzará en 1
	let fin=1;
	if(jsonActual.length===0){
		fin =1;
	}
	else if(jsonActual.length>=10){
		fin =(inicio + resultadosPorPagina);
	}else{
		fin=jsonActual.length;
	}

	const datosPagina = jsonActual.slice(inicio, fin);
	
	renderizarTabla(datosPagina, terminoBusqueda);
	actualizarEstadoBotones(pagina, Math.ceil(jsonActual.length / resultadosPorPagina));

};

// Inicialmente, muestra la primera página
document.addEventListener("DOMContentLoaded", async ()=>{
	//conseguir datos aquí
	try{
		await fetch('./cursos.json')
		.then((response) => response.json())
		.then((data) => {
			jsonData.push(...data);	
		})
		.catch((error) => {
			console.error('Error fetching or parsing JSON:', error);
		});
	}catch{
	
	}

	jsonData=jsonData.filter(item =>
		(item["Referencia Pedagógica"]["Introduccion"].trim() !== "") &&
		(item["Referencia Temática"].trim() !== "")&&
		(item["Referencia Temática"].trim() !== "URL:")
	  )
	  jsonActual=jsonData;
	cambiarPagina(paginaActual,jsonActual);
});

// Función para crear la paginación en un div al final de la tabla
const crearPaginacion = () => {
	const divPaginacion = document.createElement('div');
	divPaginacion.id = 'paginacion';
  
	// Agrega botones "Anterior" y "Siguiente"
	const botonAnterior = document.createElement('button');
	botonAnterior.textContent = 'Anterior';
	botonAnterior.addEventListener('click', () => {
	  if (paginaActual > 1) {
		paginaActual--;
		cambiarPagina(paginaActual,jsonActual, palabraBuscada);
	  }
	});
  
	const botonSiguiente = document.createElement('button');
	botonSiguiente.textContent = 'Siguiente';
	botonSiguiente.addEventListener('click', () => {
	  const maxPaginas = Math.ceil(jsonActual.length / resultadosPorPagina);
	  if (paginaActual < maxPaginas) {
		paginaActual++;
		cambiarPagina(paginaActual, jsonActual, palabraBuscada);
	  }
	});
	botonAnterior.id = 'anterior';
  	botonSiguiente.id = 'siguiente';
	divPaginacion.appendChild(botonAnterior);
	divPaginacion.appendChild(botonSiguiente);
  
	// Agrega el div de paginación al final de la tabla
	const tabla = document.querySelector('#tablaMaster');
	tabla.parentNode.appendChild(divPaginacion);
	// Actualiza el estado de los botones inmediatamente después de crearlos
	actualizarEstadoBotones(paginaActual, Math.ceil(jsonActual.length / resultadosPorPagina));
  };
  
// Llama a la función para crear la paginación
document.addEventListener("DOMContentLoaded", ()=> crearPaginacion());

// Agrega eventos a los botones de navegación
const botonAnterior = document.querySelector('#anterior');
const botonSiguiente = document.querySelector('#siguiente');
  
const actualizarTablaConResultados = (resultadosFiltrados, terminoBusqueda) => {
	paginaActual = 1;
	palabraBuscada=terminoBusqueda
	// Asumiendo que los estilos de encabezado ya están aplicados y no necesitan ser reaplicados cada vez.
	cambiarPagina(paginaActual,resultadosFiltrados, palabraBuscada);
};

const resultadosFiltrados = jsonData.filter(item => {
    // Función para buscar en propiedades, incluyendo objetos anidados
    const buscarEnPropiedades = (objeto, textoBusqueda) => {
        return Object.values(objeto).some(valor => {
            if (typeof valor === 'object' && valor !== null) {
                // Si el valor es un objeto, busca recursivamente dentro de él
                return buscarEnPropiedades(valor, textoBusqueda);
            } else {
                // Convierte el valor a cadena y lo pasa a minúsculas
                let valorTexto = String(valor).toLowerCase();
                // Elimina las frases específicas de "Nivel 1 Lección 1:" o variantes
                valorTexto = valorTexto.replace(/nivel\s*\d+\s*lección\s*\d+:/g, '');
                // Verifica si el texto modificado contiene el texto de búsqueda
                return valorTexto.includes(textoBusqueda.toLowerCase());
            }
        });
    };

    // Busca en todas las propiedades del objeto, incluidas las anidadas
    return buscarEnPropiedades(item, textoDeBusqueda);
});

const buscarEnjsonData = () => {
	
	// Obtén el texto de búsqueda
	let textoDeBusqueda = document.getElementById('input_text').value.toLowerCase().trim();
	// Filtra jsonActual para encontrar elementos que coincidan con el texto de búsqueda
	
	const resultadosFiltrados = jsonData.filter(item => {
		// Función para buscar en propiedades, incluyendo objetos anidados
		const buscarEnPropiedades = (objeto, textoBusqueda) => {
			return Object.values(objeto).some(valor => {
				if (typeof valor === 'object' && valor !== null) {
					// Si el valor es un objeto, busca recursivamente dentro de él
					return buscarEnPropiedades(valor, textoBusqueda);
				} else {
					// Convierte el valor a cadena y lo pasa a minúsculas
					let valorTexto = String(valor).toLowerCase();
					// Elimina las frases específicas de "Nivel 1 Lección 1:" o variantes
					valorTexto = valorTexto.replace(/nivel\s*\d+\s*lección\s*\d+:/gi, '');
					
					// Verifica si el texto modificado contiene el texto de búsqueda
					return valorTexto.includes(textoBusqueda.toLowerCase());
				}
			});
		};
	
		// Busca en todas las propiedades del objeto, incluidas las anidadas
		return buscarEnPropiedades(item, textoDeBusqueda);
	});
	
	
	if(textoDeBusqueda==="") {
		jsonActual=jsonData;
	}
	else{
		// actualiza el contenido del jsonActual con los resultados coincidentes
		jsonActual=resultadosFiltrados;
	}
		
	actualizarTablaConResultados(jsonActual, textoDeBusqueda)
	
  };

const manejarKeypressEnInput = (evento) => {
	// Verifica si la tecla presionada es 'Enter'
	if (evento.key === 'Enter') {
		// Evita que se ejecute la acción predeterminada de la tecla Enter
		evento.preventDefault();

		// Llama a la función de búsqueda
		buscarEnjsonData();
	}
};

const reiniciarTabla = () => {
	jsonActual=jsonData;
	paginaActual=1;
	cambiarPagina(paginaActual,jsonActual);
  };

// Función asíncrona para esperar a que crear_buscador se complete antes de agregar el evento al botón
const inicializarBuscador = () => {
	crear_buscador(); 

	// Ahora que crear_buscador ha terminado, se puede agregar el evento al botón de búsqueda
	const searchButton = document.getElementById('searchButton');
	searchButton.addEventListener('click', buscarEnjsonData);

	// Agrega el evento de clic al botón de limpiar
	const clearButton = document.getElementById('clearButton');
	clearButton.addEventListener('click', () => {
		
		// Limpia el campo de entrada
		const inputText = document.getElementById('input_text');
		inputText.value = '';

		// Reiniciar la tabla a su estado original
		reiniciarTabla();
	});


	 // Agrega el evento de teclado al campo de entrada de texto
	 const inputText = document.getElementById('input_text');
	 inputText.addEventListener('keypress', manejarKeypressEnInput);
};
  
// Evento para cuando el DOM esté completamente cargado
document.addEventListener('DOMContentLoaded', inicializarBuscador);  


const nodeDecoration = document.getElementById("nodeDecoration");

// Función para manejar el cambio de tamaño del elemento "nodeTitle"
const handleNodeTitleResize= () => {
	const article= document.querySelector('article')
    const nodeDecorationHeight = nodeDecoration.offsetHeight; // Obtener la altura del elemento en px
	
    // Calcular el margin-top deseado (4rem + altura del elemento)
    const marginTopInRem = 4 + (nodeDecorationHeight / 16) -3; // Suponiendo que 1rem = 16px

    // Verificar si el tamaño de fuente es mayor a 41px
    if (nodeDecorationHeight > 41) {
        // Aplicar el nuevo margin-top en rem
        article.style.marginTop = `${marginTopInRem-3}rem`;
    } else {
        // Restablecer el margin-top si el tamaño de fuente es menor o igual a 41px
        article.style.marginTop = "1rem";
    }
}



// Función que se ejecutará cuando se detecten cambios en el elemento "siteNav"
const handleMutations=(mutationsList) => {
	const siteNav = document.getElementById('siteNav');
	const nodeDecoration = document.getElementById('nodeDecoration');
	const header = document.getElementById('header');


    for(let mutation of mutationsList) {
        // Verifica si el atributo "style" ha cambiado
        if (mutation.attributeName === 'style'|| (mutation.attributeName === 'orientation' && mutation.type === 'attributes')) {
			const isPortrait = window.matchMedia("(orientation: portrait").matches;
			const isSiteNavHidden = window.getComputedStyle(siteNav).display === 'none';
			const valor = window.getComputedStyle(nodeDecoration);
            // const siteNav = mutation.target;
            // Verifica si el valor de "display" es "none"
            if (isSiteNavHidden) {
				if(isPortrait){
					const screenHeight = screen.height;
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					//siteNav inactivo, orientación vertical
					// Si es "none", modifica el valor de "left" del elemento "header" con id "nodeDecoration"
					nodeDecoration.style.left = '1rem';
					nodeDecoration.style.top = `${calculatedTop}px`;
					 
				}else{
					const screenHeight = screen.height;
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					//siteNav inactivo y orientación horizontal
					nodeDecoration.style.left = '1rem';
					nodeDecoration.style.top = `${calculatedTop}px`;					
				}
                
                // const nodeDecoration = document.getElementById('nodeDecoration');
                // nodeDecoration.style.left = '1rem';
				
            }
			else{
				if(isPortrait){
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					// siteNav activo, orientación vertical
					nodeDecoration.style.left = 'calc(16rem + var(--tamano-body)*0.06)';
                    nodeDecoration.style.top = `${calculatedTop}px`;
				}else{
					const headerStyles = window.getComputedStyle(header);
					const headerHeight = parseFloat(headerStyles.height);
					const calculatedTop = headerHeight + 20;
					// siteNav activo, orientación horizontal
					nodeDecoration.style.left = "calc(16rem + var(--tamano-body)*0.06)";
                    nodeDecoration.style.top = `${calculatedTop}px`;
				}
				// const nodeDecoration = document.getElementById('nodeDecoration');
			}
        }
    }
}

// Selecciona el elemento con id "siteNav"
const siteNav = document.getElementById('siteNav');

// Crea una nueva instancia de MutationObserver
const observer = new MutationObserver(handleMutations);

// Configura el observer para escuchar cambios en el atributo "style"
observer.observe(siteNav, { attributes: true, attributeFilter: ['style'] });

// Agregar un evento de cambio de tamaño a window para manejar el cambio de tamaño de nodeTitle
window.addEventListener("resize", handleNodeTitleResize());


/* PINTAR EL HEADER DE COLOR BLANCO CUANDO SE HAGA UN SCROLL HACIA ABAJO */

const handleScroll = () =>{
	const nodeDecoration = document.getElementById('nodeDecoration');
	const targetScroll = 10;

	if (document.getElementById('main').scrollTop >= targetScroll) {
		nodeDecoration.style.background = 'white';

	} else {
		nodeDecoration.style.background = 'none';
	}
}

document.getElementById('main').addEventListener('scroll', handleScroll);

const mainTopCalc=()=>{
	document.getElementById('main').style.top = 0;
}
// document.addEventListener("DOMContentLoaded", mainTopCalc);

/**
 * Scroll Persistence for #siteNav
 * 
 * Este script permite que, al seleccionar un enlace dentro de #siteNav,
 * la posición de desplazamiento se guarde en localStorage. Al recargar
 * la página, el nav se desplazará automáticamente a la posición guardada,
 * mostrando el enlace seleccionado en primer lugar.
 */
document.addEventListener("DOMContentLoaded", () =>{
    // Obtener el elemento siteNav
    var siteNav = document.getElementById("siteNav");

    // Si existe siteNav y localStorage tiene la posición guardada, desplázate a esa posición
    if (siteNav && localStorage.getItem("scrollPosition")) {
        siteNav.scrollTop = localStorage.getItem("scrollPosition");
    }

    // Agregar un evento de clic a todos los enlaces dentro de siteNav
    var navLinks = siteNav.querySelectorAll("a");
    navLinks.forEach(function(link) {
        link.addEventListener("click", function() {
            // Al hacer clic en un enlace, almacena la posición de desplazamiento actual en localStorage
            localStorage.setItem("scrollPosition", siteNav.scrollTop);
        });
    });
});

const handleLicense =() => {
	const packageLicense = document.getElementById("packageLicense");
  
	const setMarginBottom = () => {
	  const isPortrait = window.matchMedia("(orientation: portrait)").matches;
	  if (isPortrait) {
		//Si es vertical marginbottom es 8.5em
		packageLicense.style.marginBottom = '12rem';
	  } else {
		//Si es horizontal marginbottom es 10.5em
		packageLicense.style.marginBottom = '10.5em';
	  }
	};
  
	setMarginBottom(); 
  
	window.addEventListener('resize', setMarginBottom);
	window.addEventListener('orientationchange', setMarginBottom);
  }
  
handleLicense(); // Llamada inicial para establecer el margen de acuerdo a la orientación inicial


/*!
 * ScrewDefaultButtons v2.0.6
 * http://screwdefaultbuttons.com/
 *
 * Licensed under the MIT license.
 * Copyright 2013 Matt Solano http://mattsolano.com
 *
 * Date: Mon February 25 2013
 */(function(e,t,n,r){var i={init:function(t){var n=e.extend({image:null,width:50,height:50,disabled:!1},t);return this.each(function(){var t=e(this),r=n.image,i=t.data("sdb-image");i&&(r=i);r||e.error("There is no image assigned for ScrewDefaultButtons");t.wrap("<div >").css({display:"none"});var s=t.attr("class"),o=t.attr("onclick"),u=t.parent("div");u.addClass(s);u.attr("onclick",o);u.css({"background-image":r,width:n.width,height:n.height,cursor:"pointer"});var a=0,f=-n.height;if(t.is(":disabled")){a=-(n.height*2);f=-(n.height*3)}t.on("disableBtn",function(){t.attr("disabled","disabled");a=-(n.height*2);f=-(n.height*3);t.trigger("resetBackground")});t.on("enableBtn",function(){t.removeAttr("disabled");a=0;f=-n.height;t.trigger("resetBackground")});t.on("resetBackground",function(){t.is(":checked")?u.css({backgroundPosition:"0 "+f+"px"}):u.css({backgroundPosition:"0 "+a+"px"})});t.trigger("resetBackground");if(t.is(":checkbox")){u.on("click",function(){t.is(":disabled")||t.change()});u.addClass("styledCheckbox");t.on("change",function(){if(t.prop("checked")){t.prop("checked",!1);u.css({backgroundPosition:"0 "+a+"px"})}else{t.prop("checked",!0);u.css({backgroundPosition:"0 "+f+"px"})}})}else if(t.is(":radio")){u.addClass("styledRadio");var l=t.attr("name");u.on("click",function(){!t.prop("checked")&&!t.is(":disabled")&&t.change()});t.on("change",function(){if(t.prop("checked")){t.prop("checked",!1);u.css({backgroundPosition:"0 "+a+"px"})}else{t.prop("checked",!0);u.css({backgroundPosition:"0 "+f+"px"});var n=e('input[name="'+l+'"]').not(t);n.trigger("radioSwitch")}});t.on("radioSwitch",function(){u.css({backgroundPosition:"0 "+a+"px"})});var c=e(this).attr("id"),h=e('label[for="'+c+'"]');h.on("click",function(){u.trigger("click")})}if(!e.support.leadingWhitespace){var c=e(this).attr("id"),h=e('label[for="'+c+'"]');h.on("click",function(){u.trigger("click")})}})},check:function(){return this.each(function(){var t=e(this);i.isChecked(t)||t.change()})},uncheck:function(){return this.each(function(){var t=e(this);i.isChecked(t)&&t.change()})},toggle:function(){return this.each(function(){var t=e(this);t.change()})},disable:function(){return this.each(function(){var t=e(this);t.trigger("disableBtn")})},enable:function(){return this.each(function(){var t=e(this);t.trigger("enableBtn")})},isChecked:function(e){return e.prop("checked")?!0:!1}};e.fn.screwDefaultButtons=function(t,n){if(i[t])return i[t].apply(this,Array.prototype.slice.call(arguments,1));if(typeof t=="object"||!t)return i.init.apply(this,arguments);e.error("Method "+t+" does not exist on jQuery.screwDefaultButtons")};return this})(jQuery);

 