const fs = require('fs');
const readline = require('readline');

let jsonStruct = {
    "id": 0,
    "Area": "",
    "SubArea": "",
    "Curso": "",
    "Referencia Pedagógica": {
        "URL": "",
        "ServerEscuela": "",
        "Introduccion": "",
    },
    "Referencia Temática": "",
};

let lista_cursos = [];
let id = 0;
let Area=[];
let SubArea=[];

const convertirTextoAJson = (texto) => {
    id++;
    const json = JSON.parse(JSON.stringify(jsonStruct)); // Ahora es una copia profunda
    json["id"]= id;
    const lineas = texto.trim().split('\n');
    const nombreCurso = lineas[0];
    const partes = nombreCurso.split(/(?<=^\S+)\s/); // Divide en el primer espacio después de una secuencia sin espacios al inicio
    json['Curso'] = partes[1].trim();

    const partesSeparadasPorPuntos=partes[0].split(".");
    const cursoArea=`${partesSeparadasPorPuntos[0]}.`
    const cursoSubArea=`${partesSeparadasPorPuntos[0]}.${partesSeparadasPorPuntos[1]}`

    let esIntro=false;
    let esRef=false;
    
    for (const a of Area) {
        
        if(a.includes(cursoArea)){
            json['Area'] = a.split(/\.|\(/ )[1].trim();
            break;
        }
    }
    for (const sA of SubArea) {
        
        if(sA.includes(cursoSubArea)){
            json['SubArea'] = sA.split(/^\d+\.\d+|\(/)[1].trim();
            break;
        }
    }
    
    lineas.forEach((linea) => {
        if (linea.startsWith('URL: ')) {
            json['Referencia Pedagógica']['URL'] = linea.replace('URL: ','').trim();
        }
        else if(linea.startsWith('Server Escuela:')){
            json['Referencia Pedagógica']['ServerEscuela'] = linea.replace('Server Escuela:','').trim();
        }
        else if(linea.startsWith('Introducción:')) { 
            json['Referencia Pedagógica']['Introduccion']=linea.replace('Introducción:','').trim()+" ";     
            esIntro = true;
        } 
        else if(linea.startsWith('Referencia Temática')){
            esIntro = false;
            json['Referencia Temática']=linea.replace('Referencia Temática','').trim()+" ";
            esRef=true;
        }
        else if(esIntro){
            json['Referencia Pedagógica']['Introduccion']+=linea.trim()+" "
        }   
        
        else if(esRef){
            json['Referencia Temática']+=linea.trim()+" ";
        }
    });
    return json;
};

const procesarTexto =  (texto) => {
    let lineas = texto.trim().split('\n');
    let seccionActual = ""; // Almacena el texto de la sección actual
    for (let linea of lineas) {
        
        if ((/^\d+\.\d+\.\d+\s/).test(linea)){

            // Procesar la sección actual antes de comenzar una nueva
            if (seccionActual!=="") {     
                const resultado = convertirTextoAJson(seccionActual);
                lista_cursos.push(resultado);                
                seccionActual = "";
            }
            seccionActual += linea + "\n";
        }
        else{
            if(linea.trim()!==""){
                // Agregamos la línea a la sección actual.
                seccionActual += linea + "\n";
            }
        }

    }
    

    // Procesamos la última sección después de terminar el bucle.
    if (seccionActual) {
        const resultado = convertirTextoAJson(seccionActual);
        lista_cursos.push(resultado); // Agrega el objeto JSON al arreglo
    }
};

const preprocesarArchivo = async (rutaArchivo) => {
    let contenidoNuevo = ''; // Para almacenar el contenido del archivo sin las líneas a eliminar

    const fileStream = fs.createReadStream(rutaArchivo, { encoding: 'utf8' });
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    for await (const linea of rl) {
        // Validar si la línea comienza con alguno de los patrones "1. " o "1.1 "
       
        if (/^\d+\.\s/.test(linea)|| linea.includes("1. ADMINISTRACIÓN Y FINANZAS (19 CURSOS)")) {
            
            Area.push(linea); // Agregar al array Area
        // Validar si la línea comienza con el patrón para SubÁrea, ejemplo: "1.1 " o "1.1 "
        } else if (/^\d+\.\d+\s/.test(linea)) {

            SubArea.push(linea); // Agregar al array SubArea
        } else {
            contenidoNuevo += linea + '\n'; // Agregar la línea al contenido nuevo si no cumple con los patrones
        }
    }
    // Cerrar el fileStream y la interfaz de readline
    rl.close();
    fileStream.close();
    
    // Crear un nuevo archivo llamado nuevo.txt con el contenidoNuevo
    // fs.writeFileSync('nuevo.txt', contenidoNuevo);

    // Llamar a la función procesarArchivo con el archivo modificado
    procesarTexto(contenidoNuevo);
};

const main = async () => {
    await preprocesarArchivo("000_Listado-cursos-apoyo-EPT-120224.txt");
    const jsonContent = JSON.stringify(lista_cursos, null, 2);
    fs.writeFileSync('cursos.json', jsonContent, 'utf8');
};

main();